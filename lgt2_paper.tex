\documentclass[aps,prl,reprint,twocolumn,superscriptaddress,amsmath,amssymb,citeautoscript]{revtex4-1}

\usepackage{graphicx}
\usepackage{bm,amsmath,amssymb}
\usepackage{mathrsfs}
\usepackage{cases}
\usepackage{color}
\usepackage{indentfirst}
\usepackage{epstopdf}
\usepackage{grffile}

\usepackage{adjustbox}
\usepackage{subfigure}

\DeclareGraphicsExtensions{.eps}
\newcommand{\sebastian}[1]{\textcolor{magenta}{[SG: #1]}}

\newcommand{\be}{\begin{equation}}
\newcommand{\ee}{\end{equation}}
\newcommand{\bea}{\begin{eqnarray}}
\newcommand{\eea}{\end{eqnarray}}
\newcommand{\la}{\langle}
\newcommand{\ra}{\rangle}
\newcommand{\ii}{\rm i}
\newcommand{\e}{\rm e}

\newcommand{\PO}{\mathcal{O}_{P}^2}
\newcommand{\SO}{\mathcal{O}_{S}^2}

\newcommand{\aU}{{\uparrow}}
\newcommand{\aL}{{\leftarrow}}
\newcommand{\aR}{{\rightarrow}}
\newcommand{\aD}{{\downarrow}}
\newcommand{\aH}{{\circ}}
\newcommand{\aP}{{\bullet}}

\newcommand{\vR}{\vec{r}}
\newcommand{\vRp}{\vec{r'}}
\newcommand{\vX}{\vec{x}}
\newcommand{\vY}{\vec{y}}

\newcommand{\tr}{\rm Tr}

\renewcommand\vec{\mathbf}


\newcommand{\CState}[4]{\arraycolsep=0.5pt\def\arraystretch{0.6}\scriptsize
\begin{array}{ccc}
{#1} & {#2} & {#4} \\
 & {#3} &
\end{array}}

\newcommand{\curleq}[2]{%
\begin{array}{#1}#2\end{array}}


\newcommand{\hannover}{Institut f\"ur Theoretische Physik, Leibniz Universit\"at Hannover, Appelstr. 2, DE-30167 Hannover, Germany}

\begin{document}
\title{Deconfining disordered phase in two-dimensional quantum link models}
\author{Lorenzo Cardarelli} 
\affiliation{\hannover}
\author{Sebastian Greschner} 
\affiliation{Department of Quantum Matter Physics, University of Geneva, CH-1211 Geneva, Switzerland} 
\author{Luis Santos}
\affiliation{\hannover} 

\date{\today}
\begin{abstract}
We explore the ground-state physics of two-dimensional spin-$1/2$ $U(1)$ quantum link models, one of the simplest non-trivial lattice gauge theories 
with fermionic matter within experimental reach for quantum simulations. 
Whereas in the large mass limit we observe Ne\'el-like vortex-antivortex  and striped crystalline phases, 
for small masses there is a transition from the striped phases into a disordered phase whose properties resemble those at the Rokhsar-Kivelson point of the quantum dimer model. 
This phase is characterized on ladders by boundary Haldane-like properties, such as vanishing parity and finite string ordering.
Moreover, from studies of the string tension between gauge charges, we find that whereas the striped phases are confined, the novel disordered phase present clear indications of being deconfined. 
Our results open exciting perspectives of studying highly non-trivial physics in quantum simulators, such as spin-liquid behavior and confinement-deconfinement transitions, 
without the need of explicitly engineering plaquette terms.
\end{abstract}
\maketitle



%%%%%%%%%%

% INTRODUCTION

Driven by tremendous progresses in the manipulation and control of ultracold quantum gases, this field is entering the era of the quantum simulation of lattice gauge theories~(LGTs)~\cite{Wilson1974}, 
with the long term goal of studying open problems of the early universe, dense neutron stars, nuclear physics or condensed-matter physics~\cite{Kogut1983, Levin2005, Wiese2013}.
Many theoretical proposals~\cite{Cirac2010,Zohar2011,Kapit2011,Zohar2012,Banerjee2012,Zohar2013,Banerjee2013,Zohar2013b,Tagliacozzo2013,Stannigel2014,Kasper2016,Cardarelli2017, Zache2018, barbiero2018} and recent seminal experiments with trapped ions~\cite{Martinez2016}, quantum dimer models in Rydberg-atoms-arrays~\cite{Bernien2017}, lattice modulation techniques~\cite{Clark2018, Goerg2019, schweizer2019}, or atomic mixtures~\cite{Mil2019} have shown first building-blocks of dynamical gauge fields and quantum link models~(QLMs), a generalization of LGT to spin-like link-variables~\cite{Chandrasekharan1997}.
However, the implementation of some building blocks of LGT, such as the ring-exchange corresponding to magnetic field dynamics in analogue implementations of quantum electrodynamics, 
requires further theoretical and experimental breakthroughs, although there has been progress on isolated plaquettes~\cite{dai2017four} and recent promising proposals~\cite{Celi2019, Bohrdt2019}.
% Here there is also a paper of Marcello (Surace et al) on the re-interpretation of Rydberg experiments. We should cite that.

In this paper we show how already the simplest mid-term experimental realizations, without plaquette terms, may be able to explore a wide area of non-trivial phenomena of LGTs. In particular, we report in this Letter that 
the two-dimensional~(2D) QLM is characterized by the emergence of a quantum phase transition between confined crystalline phases and an exotic deconfined disordered phase with certain resemblance to Rokshar-Kivelson~(RK) states~\cite{rokhsar1988} or resonating valence bond liquids~\cite{Shastry1981,Anderson1987, Moessner2002, Ralko2005}. Hence, these relatively simple systems provide a pristine test-bed for 
the study of highly non-trivial physics, such as spin liquids, confinement-deconfinement transitions, and exotic dynamical or thermalization properties~\cite{Bernien2017, Brenes2018, feldmeier2019emergent}, such as the formation of quantum many-body scars in constrained systems~\cite{turner2018scars} and their fundamental link to confinement. Interestingly, QLMs may be experimentally realized in quantum gases within the next years. 
Whereas several proposals using Fermi-Bose mixtures have been reported~\cite{Banerjee2012,Zohar2012,Banerjee2013,Stannigel2014,Kasper2016,Zache2018,Mil2019}, we recently discussed~\cite{Cardarelli2017} a minimalistic realization of QLMs with a single fermionic species 
that simulates the spin-$1/2$ links using multi-orbital physics in optical superlattices~\cite{Wirth2011}. We note, however, that the latter may be analogously replaced by hyper-fine or spatial degrees of freedom allowing for a large flexibility of the proposal.


%%%%%%%%%%%%

% FIGURE 1

\begin{figure}[t]
\centering
\includegraphics[width=1.\linewidth]{fig1.pdf}
\caption{(a) Sketch of the QLM model on a square ladder~(gray spins depict staggered boundary conditions) or on a torus (associating opposite gray spins at the two edges). 
(b-f) Sketches of the phases discussed in the text: for $\mu=0.8$ striped phases (b) $J_y=2.4 J_x$~(Sy), (c) $J_y=0.2 J_x$~(Sx); 
for $\mu=-0.8$ vortex-antivortex phases (d) $J_y=2.4 J_x$~(VA)  (e) $J_y=0.2 J_x$~(VA$'$), and the (f) disordered deconfined D phase for $\mu=0$ and $J_x=J_y$. The size of the bullets depicts 
$\la n_{\vR}\ra$, the arrow size $\la S_{\vR,\vRp}^z\ra$, and the plaquette colors the vorticity $\la Q_{\vR}\ra$. The arrow sizes of (f) has been scaled up by a factor of $2$ for clarity.}
\label{fig:sketch}
\end{figure}

%%%%%%%%%%%%


%%%%%%%%%%%%

% FIGURE 2
% We should change the order of the figures a <-> b, and c <-> d

\begin{figure*}[t]
\centering
\includegraphics[width=0.99\linewidth]{fig2.pdf}
\caption{Four-leg cylinder at $\mu=0$: 
(a) Nearest and next-nearest neighbor flippability correlations, staggered flippability $O(Sy) = \sum_\vR (-)^x \la Q_{\vR}^2 \ra$, as well as the expectation value of the ring-exchange $\la R^+_{\vR} \ra$.
(b) Fidelity susceptibility with $L=12$, $24$ and $36$ rungs.
(c) Local Hilbert space distribution $\nu_k$ of the central rung~(see text).
(d) Sketched phase diagram of the QLM. Color codes depict the von-Neumann bipartite entanglement entropy $S_{vN}$ of the central rung. 
Points depicts the estimated phase transition points, by extrapolating the peak positions of the fidelity susceptibility evaluated along the cuts indicated by the dotted lines (DMRG data).}
\label{fig:4t_cuts_mu0}
\end{figure*}

%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%

% 2D QLM

\paragraph{2D QLM.--} We consider in the following a QLM on a square lattice described by the Hamiltonian
\begin{align}
H =  - \sum_{\vR,\vRp} J_{\vR,\vRp} \left( \psi_\vR^\dagger S^+_{\vR,\vRp} \psi_{\vRp} +  {\rm h.c.} \right)
+ \sum_\vR \mu_\vR n_\vR, 
\label{eq:QLM}
\end{align}
% mu_r=mu for A and -mu for B. For mu>>0 piling on B, this means that B goes with -mu
where $\psi_\vR^\dagger$ is the fermionic operator at site $\vR$, and $n_\vR = \psi_\vR^\dagger \psi_\vR$. 
In our case the gauge field is given by spin-$1/2$ operators $S^+_{\vR,\vRp}$ placed at the link between {{two neighbouring}} sites $\vR$ and $\vRp$. The 
amplitudes $J_{\vR,\vR\pm\vX}=J_x$ and $J_{\vR,\vR\pm\vY}=J_y$ characterize, respectively, the hops along the $x$ and $y$ directions~(Fig.~\ref{fig:sketch}~(a)). 
We enforce at each site the Gauss law $[H, G_\vR]=0$ with $G_\vR = \varepsilon_\vR - n_\vR + \sum_{\vec{k} \in \vX,\vY} ( S^z_{\vR,\vR+\vec{k}} - S^z_{\vR,\vR-\vec{k}} )$,
with $\varepsilon_{\vR\in A}=1$ and $\varepsilon_{\vR\in B}=0$. 
The staggered potential, $\mu_{\vR\in A}=\mu$ and $\mu_{\vR\in B}=-\mu$ can be interpreted as the mass of particles on B sites and anti-particles on A sites. 

We focus on the ground-states of the QLM at half fermion filling and gauge vacuum~($G_\vR=0$) on square ladders and cylinders. We study up to $L_x=100$ rungs and $L_y=4$ legs 
by means of density matrix renormalization group~(DMRG) techniques~\cite{Schollwock2011} adapted to the local gauge symmetry~\cite{Dalmonte2016cont, Tschirsich2019, Chepiga2019dmrg, DMRGnote}.
We introduce at this point the ring-exchange operators $R^+_\vR= S_{\vR,\vR+\vX}^+ S_{\vR+\vX,\vR+\vX+\vY}^- S_{\vR+\vX+\vY,\vR+\vY}^- S_{\vR+\vY,\vR}^+$ and $R_\vR^-=(R_\vR^+)^\dagger$.
These operators characterize plaquette states: $R_\vR^-$~($R_\vR^+$) flips a vortex~(antivortex) into an antivortex~(vortex), being zero otherwise, 
and $Q_{\vR}=(R_\vR^+R_\vR^- - R_\vR^-R_\vR^+)=1$~(-1) for vortex~(antivortex) and $Q_{\vR}=0$ otherwise~\cite{Arrows}.
%, and  $R_\vR^2 = Q_\vR^2$ is $1$~($0$) for flippable~(unflippable) plaquettes.
%Note, however, that in contrast to QED realizations or to the quantum dimer model~(QDM), they do not appear explicitly in the Hamiltonian~\eqref{eq:QLM}.


%%%%%%%%%%%%%%%%%%%%%%%%%

% LARGE MASS

{\em Large mass limit.--} First insights are obtained from the limit $|\mu| \gg J_{x,y}$, which, in contrast to the two-leg QLM~\cite{supmat}, is different 
%~\nocite{aklt, Pollmann2010, Pollmann2012, Pollmann2012A}
for $\mu>0$ and $\mu<0$. For $\mu>0$, particles 
are pinned in B sites~(Figs.~\ref{fig:sketch}~(b) and (c)). 
Local states are characterized by the expectation value of two spin-$1$ operators, $S^z_{\vec{k}}(\vR) = S^z(\vR-\vec{k}, \vR) + $
{{$S^z(\vR,\vR+\vec{k})$}},
with $\vec{k}=\vX,\vY$. 
For $J_x<J_y$, second-order terms select a ground-state manifold of two states with $S^z_y(\vR)=0$. Fourth-order ring-exchange $\propto J_x^2 J_y^2 / |\mu|^3$~\cite{Cardarelli2017} 
favors a configuration of columns of flippable vortex-antivortex and non-flippable plaquettes~(Fig.~\ref{fig:sketch}~(b)). We denote this striped phase Sy. 
In a 2D model, a corresponding striped phase Sx of alternating flippable and non-flippable rows of plaquettes is expected for $J_y<J_x$. 
However, on the finite-size cylinders we study,
%the unbroken translational symmetry
the translational symmetry along the \textit{y} direction
results in blurred spin averages~(Fig.~\ref{fig:sketch}~(c)). 
Correlations of the flippability operators reveal the Sx character~(Fig.~\ref{fig:4t_cuts_mu0}~(a)):  whereas $\la Q_{\vR}^2 Q_{\vR+\vY}^2 \ra$ vanishes, $\la Q_{\vR}^2 Q_{\vR+2\vY}^2 \ra$ remains finite.
Staggered boundary spins stabilize the Sx ordering in ladders~\cite{supmat}.

For large negative mass, $-\mu \gg J_{x,y}$, particles are pinned to the A-sites, reducing the local Hilbert space to a six-dimensional manifold of spin configurations satisfying Gauss' law.
Second-order processes favor states with $\la S^z_{\mathbf{x}}(\vR) \ra=\la S^z_{\mathbf{y}}(\vR) \ra=0$, leading to a checkerboard ground-state pattern of vortex-antivortex~(VA) plaquettes~(Figs.~\ref{fig:sketch}~(d) and (e)). For $J_x<J_y$ we dub this phase VA, and VA$'$ for $J_y<J_x$. These two phases are uniquely defined and do not exhibit any spontaneously broken translational symmetry like in a Ne\'el-like phase.


%%%%%%%%%%%%%%%%%%%

{\em Emerging disordered phase.--}  At low $\mu$ particle fluctuations become important, leading to a particularly intriguing physics.
For $\mu\sim 0$ we observe three distinct phases as a function of $J_y/J_x$, as can be seen in Fig.~\ref{fig:4t_cuts_mu0}~(b) for the four-leg cylinder 
by the distinct diverging peaks in the fidelity susceptibility $\chi_{FS} = \lim_{J_y-J_y' \to 0} \frac{-2 \ln |\langle \Psi_0(J_y) |\Psi_0(J_y') \rangle| }{(J_y-J_y')^2}$,
where $|\Psi_0\rangle$ is the ground-state wave-function. We observe a similar behavior for three- and four-leg ladders~\cite{supmat}. 
Whereas for $\mu=0$ for $J_y \ll J_x$~($J_y \gg J_x$) the system is in the Sx~(Sy) phase, for $J_x\sim J_y$ an intermediate gapped phase occurs characterized by vanishing 
$\la Q_{\vR} \ra$ and $\la Q_{\vR}^2 \ra$, but a large expectation value of the ring-exchange $\la R^+_{\vR} \ra$.


A crucial insight on the physics of the intermediate phase is provided by the analysis of the reduced density matrix $\rho_c = \tr |\Psi_0\ra\la \Psi_0|$ for the central rung~(where the trace runs over all other rungs) 
in the~(Fock-like) eigenbasis $\phi_k$ of $S^z_{\vR,\vRp}$ and $n_{\vR}$.
In Fig.~\ref{fig:4t_cuts_mu0}~(c) we show its diagonal elements $\nu_k = \la \phi_k | \rho_c | \phi_k \ra$, an effective local Hilbert space distribution, sorted by amplitude, for the case of a four-leg cylinder. The Sx and Sy phases are strongly localized in Fock space, i.e. $\nu_k$ has most weight for few basis states. The intermediate phase, however, exhibits a drastically different, much flatter distribution, where many local Fock states contribute with similar weight. The disordered character of the intermediate phase is also witnessed by the entanglement entropy $S_{vN}=-\tr(\rho_c \ln \rho_c)$, which we depict in Fig.~\ref{fig:4t_cuts_mu0}~(d).


The intermediate phase thus closely resembles the Rokhsar-Kivelson~(RK) point, which contains an equal superposition of all dynamically connected states. We also show in Fig.~\ref{fig:4t_cuts_mu0}~(c) the corresponding distribution of  $\nu_k$ for a classical RK state, which compares well to the ground state obtained by the DMRG simulation. We estimate the overlap between the two states to 
be $0.97$~(see~\cite{supmat} for a more detailed comparison between the DMRG simulation of the intermediate phase and the classical RK state, which also reproduces the spin and density configuration pattern of Fig.~\ref{fig:sketch}~(f)). We hence characterize the intermediate gapped phase as a disordered~(D) phase. Note, that due to the different Gauss' law on A and B sites, this phase still exhibits a slight particle imbalance between A and B sites, as well as finite link-variable expectation values, as shown in Fig.~\ref{fig:sketch}~(f).


%%%%%%%%%

% FIGURE 3

\begin{figure}[tb]
\begin{minipage}[t]{.03\linewidth}
\raisebox{2.8cm}[0cm][0cm]{(a)}
\end{minipage}
\begin{minipage}[t]{.45\linewidth}
	\includegraphics[height=2.7cm]{fig3a.pdf}
\end{minipage}
\begin{minipage}[t]{.03\linewidth}
\raisebox{2.8cm}[0cm][0cm]{(b)}
\end{minipage}
\begin{minipage}[t]{.45\linewidth}
	\includegraphics[height=2.7cm]{fig3b.pdf}
\end{minipage}
\caption{(a) Parity and string order along the boundary legs of a four-leg ladder, obtained using DMRG for $L_x=100$ rungs and $\mu=0$; (b) largest eigenvalues of the entanglement spectrum, obtained 
after dividing the system into two parts along the central rung.}
\label{fig:entanglement_SO}
\end{figure}

%%%%%%%%%



%%%%%%%%%%%%%%%%

% EDGE HALDANE ORDER

\paragraph{Edge Haldane order.--} We focus at this point on the edges of a QLM ladder, where the physics can be well understood from 
a mean-field-like strongly simplified 1D model in which we fix the upper boundary links for each site in a staggered configuration, and 
allow the lower spins to fluctuate with an amplitude $J_y$. Six local states are possible:
$\left|0_\pm \right\rangle = ( \left| \CState{\aL}{\aH}{\aU}{\aL} \right\rangle \pm \left| \CState{\aL}{\aP}{\aD}{\aL} \right\rangle ) /\sqrt2$,
$\left|\tilde{0}_\pm\right\rangle = ( \left| \CState{\aR}{\aH}{\aU}{\aR} \right\rangle \pm \left| \CState{\aR}{\aP}{\aD}{\aR} \right\rangle) /\sqrt2$,
$\left|\alpha \right\rangle = \left| \CState{\aR}{\aH}{\aD}{\aL} \right\rangle$, and 
$\left|\beta \right\rangle = \left| \CState{\aL}{\aP}{\aU}{\aR} \right\rangle$.
Gauss' law imposes further restrictions on the allowed sequence of these local states: $0$ may be followed at its right by $0$ or $\beta$~($0\to 0,\beta$); $\tilde{0} \to \tilde{0}, \alpha$; $\alpha \to \beta, 0$; and 
$\beta \to \alpha, \tilde{0}$~(we remove the $\pm$ index). By construction, Gauss' law enforces a Ne\'el-like order of $\alpha$ and $\beta$ states diluted by 
an arbitrary number of intermediate $0$ or $\tilde{0}$ states.
The model Hamiltonian, given by
\begin{align}
H_{\rm 1D} =  - J_x \sum_{x} \psi_{x}^\dagger S_{x,x+1}^+ \psi_{x+1} - J_y \sum_{x} \psi_{x}^\dagger S_{x}^+ + {\rm H.c.}
\label{eq:QLM1D}
\end{align}
exhibits three ground-state phases (here we neglect a staggered potential term). For $J_y\ll J_x$ the ground state is $\cdots |\alpha\ra |\beta\ra |\alpha\ra |\beta\ra \cdots$, whereas for $J_y\gg J_x$ 
the states $\cdots |0_-\ra |0_-\ra \cdots$ and $\cdots |\tilde{0}_-\ra |\tilde{0}_-\ra \cdots$ have the lowest energy. Interestingly, for $J_x\sim J_y$ an intermediate phase with Haldane-like diluted Ne\'el order emerges, 
that resembles the SPT phase of Ref.~\cite{Cardarelli2017}.
We may describe this intermediate phase by a minimal AKLT-like~\cite{aklt} state with a two-fold degenerate entanglement spectrum and a non-vanishing string order ${\cal O}_{S}^2 = \lim_{|x-x'|\to\infty} \la S^z_x \e^{\ii \pi \sum_{x<k<{x'}} S^z_k} S^z_{x'} \ra$, while parity order ${\cal O}_{P}^2 = \lim_{|x-x'|\to\infty} \la \e^{\ii \pi \sum_{x<k<{x'}} S^z_k} \ra$ is exponentially suppressed~\cite{supmat}.

While being a drastically simplified description, it captures essential ingredients of ladder QLMs. In particular, fixing in a ladder the boundary spins to a staggered configuration enforces the dilute Ne\'el order on the boundary leg. We, hence, plot in Fig.~\ref{fig:entanglement_SO}~(a) the string- and parity order measured along the boundary leg of a 4-leg ladder. Indeed the D phase is characterized by a finite string order and a vanishing parity order, resembling closely the SPT phase discussed for the above mentioned 1D model or the two-leg QLM of Ref.~\cite{Cardarelli2017}. However, for $L_y>2$ the parity order remains finite in the D phase if measured on the inner legs, and
the phase is not topological. The entanglement spectrum is no longer strictly two-fold degenerate. Interestingly, however, we observe a robust gap in the entanglement spectrum of the D phase between a low-lying manifold and the rest.


%%%%%%%%%

% FIGURE 4
\begin{figure}[tb]
\centering
\includegraphics[width=1.\linewidth]{fig4.pdf}
\caption{(a) Average fermionic density and bond configuration of two charges at a distance of $L_D=12$ sites, after substracting the charge-free configuration. We employ DMRG for a cylinder with $L_y=4$ legs and $L=36$ rungs and $\mu=0.4 J_x$. (a) Sy phase~($J_y=1.8 J_x$), (b) Sx phase~($J_y=0.4 J_x$), (c) D phase~($J_y=J_x$). (d) String tension $S_T$ as a function of the distance between the defects $L_D$.}
\label{fig:config_4T_ST}
\end{figure}

%%%%%%%%%


%%%%%%%%%%%%%%%%%%

% STRING TENSION

\paragraph{String tension.--} Finally, we discuss the properties of gauge charges on top of the vacuum state.
We insert two charges by locally adjusting Gauss' law to $G_\vR = \pm 1$ on two sites separated by a distance $L_D$ in $x$-direction, 
and study the string formation for the case of a four-leg cylinder.
Example configurations are shown in Figs.~\ref{fig:config_4T_ST}~(a)-(c) for Sy, Sx and D phases after subtracting the spin and fermion configuration of the charge-free system.
Comparing the energy $E(L_D)$ with the energy $E_0$ of the charge-free state, we obtain the string tension, $S_T(L_D) = E(L_D)-E_0$~(Fig.~\ref{fig:config_4T_ST}~(d)), 
which characterizes the confining properties~\cite{Tschirsich2019}.

Only Sy shows a clear string formation~(Fig.~\ref{fig:config_4T_ST}~(a)). The tension increases linearly 
in a staggered way due to the broken symmetry, as depicted in Fig.~\ref{fig:config_4T_ST}(d). This is a clear signature of the confinement of excitations. 
For the Sx phase the increase of potential energy is also linear and very large compared to the other phases. Here, however, after some distance the string breaks and is wrapped around the cylinder in $y$-direction (see Fig.~\ref{fig:config_4T_ST}~(b)). Also the string tension flattens after this point as shown in Fig.~\ref{fig:config_4T_ST}~(d) for $J_y/J_x=0.4$. Interestingly, the two charges become, hence, effectively deconfined due to the finite size of the system in y-direction.

In the D phase the tension grows slowly with $L_D$ and potentially finally saturates, indicating charge deconfinement.  Contrary to the Sx phase we observe in Fig.~\ref{fig:config_4T_ST}~(c) the formation of a symmetric broad but localized perturbation of the spin and charge background around the defects. 
Even though due to the limited system size we cannot distinguish the saturation of the string tension from a further slow~(e.g. logarithmic) growth, these results show that Sx, Sy and D phases exhibit drastically different confinement and deconfinement properties.



%%%%%%%%%%%%%%

% CONCLUSIONS

\paragraph{Conclusions.--} We studied the ground state of a 2D spin-$1/2$ QLM, which may be realizable in quantum gas lattice gauge simulators in the foreseeable future. 
Despite the absence of plaquette terms, 2D QLMs are characterized by a highly nontrivial physics.
%Even though key elements of true LGT, may be missing, a rich physics can be observed in the vacuum state by controlling the ratio of the hopping amplitudes in the $x$- and $y$-direction, and the staggered potential fermion mass-like term. 
As a main result, we have found an emergent deconfined disordered phase for $\mu\sim 0$ and $J_x\sim J_y$, which closely resembles an RK phase. On finite ladder systems with staggered boundary spins this phase exhibits Haldane-like ordering at the edge legs. While being limited to small transversal lengths $L_y\leq 4$, the observed features qualitatively remain robust over two-, three- and four-leg ladders and four-leg cylinders, strongly hinting that the intermediate disordered phase may survive in more general 2D lattices, which might inspire further numerical efforts in this direction.

Our results open the interesting possibility to study a wealth of phenomena such as deconfinement-confinement transition and RVB-like physics in quantum gas lattice gauge simulators, without 
the need of explicitly realizing ring-exchange and RK terms. The dynamics of these systems may be particularly interesting. Further experimental and theoretical studies should reveal the potentially unconventional thermalization properties~\cite{turner2018scars, feldmeier2019emergent} of constrained systems with fermionic matter.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{acknowledgments}
We thank Alessio Celi and Marcello Dalmonte for enlightening discussions. S.~G. acknowledges support by the Swiss National Science Foundation under Division II. 
L.~C. and L.~S. thank the support of the German Research
Foundation Deutsche Forschungsgemeinschaft (Project No. SA 1031/10-1 and the Excellence Cluster QuantumFrontiers).
Simulations were carried out on the cluster system at the Leibniz University of Hannover and the Baobab cluster of the University of Geneva.
\end{acknowledgments}

\bibliography{references}

\end{document}

