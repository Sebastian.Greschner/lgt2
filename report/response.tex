\documentclass[showpacs,preprintnumbers,amsmath,amssymb]{revtex4-1}

\usepackage{ulem}		% Part of the standard distribution
\usepackage{amsmath}
\usepackage{epsfig,psfrag}
\usepackage{dcolumn}% Align table columns on decimal point
\usepackage{bm}% bold math
\usepackage{graphicx}
\usepackage{color}
\usepackage{version}
\newcommand{\qref}[1]{``\textit{#1}''}
\newcommand{\mc}[1]{\textcolor{magenta}{\textit{ #1}}}
\newcommand{\dl}[1]{\textcolor{blue}{\textit{ #1}}}
\newcommand{\comm}[2]{\left[#1,#2\right]}
\newcommand{\ket}[1]{\left|#1\right>}
\newcommand{\bra}[1]{\left<#1\right|}
\newcommand{\av}[1]{\left<#1\right>}

\setlength\parindent{0pt}


\begin{document}
Dear Editors,

\vspace{2mm}

We hereby resubmit our revised paper ``Deconfining disordered phase in two-dimensional quantum link models'' (LL16725) for publication in Physical Review Letters. 
We would like to thank both referees for their careful reading of our manuscript and their detailed and constructive comments. We are pleased to read that both referees consider 
our work "important", "interesting for different fields of researchers", and "suitable for
publication in PRL" after some improvements. As the referees highlight, the newly discovered D-phase, which bears some resemblance to spin-liquid physics, could be very interesting 
for on-going quantum gas experiments.

\vspace{2mm}

Please find below our detailed reply to the referees. Motivated by the comments and questions of the referees, we have introduced some extensions and modifications in the resubmitted 
manuscript and in the supplementary material, which we detail below. We hope that with these modifications the paper is now suitable for publication in Physical Review Letters.

\vspace{2mm}

Yours sincerely,

\vspace{3mm}

L. Cardarelli, S. Greschner and L. Santos

\vspace{2mm}

%------------------------------------------%
%------------------------------------------%
%------------------------------------------%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LIST OF CHANGES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\noindent\makebox[\linewidth]{\rule{\textwidth}{1pt}}

List of changes

\noindent\makebox[\linewidth]{\rule{\textwidth}{1pt}}

\begin{itemize}
\item extended discussion of the 2-leg ladder model and analogies and differences to the present paper in the SM
\item updated Fig.1a
\item new section ``Relation to quantum dimer models'' in the SM
\item demonstration of the stability of the DMRG-procedure with bond-dimension  in new Fig. S6
\item excitation gap calculation for various models in new Fig. S7
\item updated discussion of the classical Metropolis sampling in the SM
\item discussion of the symmetries of the 1D-toy model and calculation of the generalized topological order parameter in the SM
\item several corrected typos, updated references
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% REFEREE A
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\noindent\makebox[\linewidth]{\rule{\textwidth}{1pt}}

Report of Referee A 

\noindent\makebox[\linewidth]{\rule{\textwidth}{1pt}}

\vspace{2mm}

{\it
This manuscript studies the ground state properties in a spin-1/2
quantum link model on a square ladder employing density matrix
renormalization group techniques. The model is remarked by quantum
phase transitions between ordered and disordered phases, while the
latter resembles Rokshar-Kivelson states. The authors evaluate various
quantities including correlation functions, fidelity susceptibility,
and bipartite entanglement entropy to depict the phases, and the
presence of phase transitions is highly convincing. The authors also
exhibit how the phases manifest themselves in edge physics and charge
excitations upon the vacuum. The model is considerably simpler than
other lattice gauge theories for experimental realization.

I think this work is important and the paper could be suitable for
publication in PRL after some improvement.}

\begin{quote}
We thank Referee A for the careful and positive evaluation of our work.  We hope that our answers and modifications will satisfy to his/her remarks. 
\end{quote}

{\it
I hope the authors can
expand their comments on the difference between the results in this
paper and those in their previous work (Ref. [16] in the manuscript).
In the previous work, the same model on the 2-leg ladder is studied
while in this manuscript the authors focus on the 4-leg ladder. Some
aspects of the two cases are similar, for example, the phase diagrams.
Although some comments have already made in the manuscript, it would
be helpful if the authors further clarify what properties are truly
two-dimensional in the 4-leg case.}

\begin{quote}
Analogies first: the VO and VA phase, proper to the two-leg case and
symmetric under mass sign exchange, generalize onto the SX/SY and VA/VA' phases for multiple-rungs ladders.
The graph of the von-Neumann entropy, Fig. 2d, is reminiscent of the order
parameter graph in the two-leg case (Fig. 2a in the previous work, Ref. [16] in the manuscript).
However, in the extended case, an emergent deconfined disordered
phase \textit{D} appears, which represents one of the main findings of this work and a crucial
element of novelty in comparison with the previous work.
What is lost in the ladder upgrade is the SPT phase, property exclusively
belonging in the two-leg ladder case.

We have added this discussion to the supplemental material and adjusted the main text.
\end{quote}

{\it
Besides, I hope the authors can modify some confusing notations.
For example, the same Hamiltonian in Ref. [16] is more legible.
}

\begin{quote}
The explicit notation adopted in the previous work would generate some extremely long indices, for instance in the ring-exchange operator.
Therefore, we opted for keeping the compact form; however, in order to facilitate the understanding of the notation, we enriched the legend in Figure 1(a).
\end{quote}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% REFEREE B
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\noindent\makebox[\linewidth]{\rule{\textwidth}{1pt}}

Report of Referee B

\noindent\makebox[\linewidth]{\rule{\textwidth}{1pt}}

\vspace{2mm}


{\it
The paper by Cardarelli et al. studies several interesting models with
gauge symmetry. They focus on the effect of the fermion hopping
induced phase diagram using DMRG simulation.

They have several interesting discoveries.

1. They found the fermion hopping can generate effective dynamical
terms similar to the four-spin interaction. The discovery is
interesting from the experiment's point of view since it could be one
of the methods to implement the ring exchange term.

2. The discovery of the low energy disordered phase is very similar to
the RK dimer liquid phase. The quantum liquid phase is a
highly-entangled quantum state which will be valuable for future
studies. The proposal could be a possible way to realize a quantum
spin liquid in synthetic quantum matter.

3. The authors also study the effective boundary Hamiltonian (2). They
found a quantum phase similar to the Haldane phase.

4. They also study the string tension in the disordered phase and
study how the string tension scales as a function of tuning parameter
Jy/Jx. In the disordered phase, the string tension drops, which mimics
the deconfined phase. In the ordered phase, the string tension
increases in a roughly linear fashion.

The results are interesting for different fields of researchers. If
the following questions can be addressed/answered by the authors
properly, I will consider the approach is solid, and the theoretical
interpretation is sound. Then, I would suggest the paper to be
published on PRL.
}

\begin{quote}
We thank Referee B for the careful and positive evaluation of our work and the detailed questions and comments.
\end{quote}


{\it
The model seems strongly related to quantum dimer models in the
frustrated magnets community. It will be nice if some information/
reference elaborates on the connection with the quantum dimer model
can be provided in the supplemental materials.
}

\begin{quote}
For the limit of  $|\mu| \gg J_x, J_y, \mu>0$, charges are pinned to the B-sublattice and the dynamics is reduced to the spins on the links. Indeed, this limit corresponds to a model of tightly packed hard-core dimers, a quantum dimer model on a square lattice. 
Traditionally, as in Refs.~[32,33,40] the dimer-model Hamiltonian contains ring-exchange terms, which - on the square lattice - have been shown to introduce several phase transitions: the phases found and discussed e.g. in Ref.[40] are the Néel, the columnar, or the plaquette-ordered (also dubbed RVB-solid) phase. At the phase-transition point between columnar and plaquette-ordered phase, lies the so called Rokhsar-Kivelson point, discussed in the paper. Here we do not have such strong ring-exchange terms (only in terms from fourth order perturbation) and the physics for the strong-coupling dimer limit is indeed different from the phase diagram of Refs.~[32,33,40]. 

We have added this discussion to the supplemental material.
\end{quote}


{\it
2. At the limit of small $\mu$ (in equation (1)), the system should
have a relatively small gap. At this limit, I will expect the fermion
hopping term dominates. Especially, At the limit of $\mu=0$, I would
expect the system is in a gapless phase. If the model is indeed a
gapless phase at $\mu=0$, the DMRG approach might need a high bond
dimension to capture the wave function correctly. Is it possible to
calculate current data with different bond dimensions and conclude the
disordered phase is robust as we increase the bond dimension? Or, is
it possible to have an analysis/argument of the energy gap and
established the robustness of the disordered phase?
}

\begin{quote}
In the supplemental material, we added a graph (the current Figure S6) showing
a clear exponential convergence of the ground-state energy as a function of the
bond dimension, for a fixed chain length.
The curves suggest that the ground-state phase diagram obtained with MPS in
manifolds with bond-dimensions up to $\chi$=400, relatively small with regard to
the local Hilbert space dimension, is qualitatively correct.

This can be understood from the fact that the system remains gapped even at small $\mu\sim 0$ where the fermion hopping dominates.
We have added a new Fig.~S7 to the supplemental material to show the properties of the excitation gap as a function of $J_y/J_x$ for the various geometries and models discussed in this text.
In particular, we find that the gap in the D-phase is of order $J_x/2$ for $J_x\sim J_y$, almost independently from the number of legs. 
\end{quote}


{\it
In the supplement, the author mentioned the classical RK state is
obtained by Metropolis sampling of classical QLM. Is it possible to
provide further details about the setup of the numerical approach? Or
provide some related reference on the wave function. Naively, I will
expect the wave function can be solved exactly at the RK point. Why
does the comparison need to be done numerically with a classical QLM
instead of comparing it with analytic results? 3-a. If I understand it
correctly, the Monte Carlo will sample states in different sectors. In
DMRG, if the algorithm is designed to keep the symmetry, the algorithm
will minimize the energy within that sector. If the state is
initialized in a random way, that means the wave function could be the
lowest energy state withing a random sector. Will that change the
result significantly? If the sector structure in the $U(1)$ QLM is not
relevant, why is it so? If it is relevant, how it changes the current
results?
}

\begin{quote}
The Metropolis sampling creates a infinite temperature (random) superposition of all classical configurations reachable by single particle-tunnelings. Actually, the sampling does not mix different symmetry sectors and we make sure that we initialize the algorithm in a state of the gauge vacuum sector compatible with DMRG-simulations and it does not leave this sector. We have extended the discussion of this procedure.

Our main problem with analytical results is that in Fig. 2c of the main text as well and Fig. S5 of the supplemental material we compare properties of the RK-like state on a finite ladder and cylinder geometry. 
Even though analytical calculations seem possible, the numerical sampling of the state seems - due the strong influence of the boundary conditions  - practically the easiest way to access the reduced density
matrix. In the future work we will try to extend our numerical DMRG method to more 2D-settings on larger cylinders, and here a comparison with analytical results will be indeed an interesting objective. 

As mentioned above, also in our Metropolis sampling we do not mix symmetry sectors. 
Initialization of the system in a random gauge symmetry sector can be interpreted as random localized charges to the problem. This can lead to interesting localization and dynamical effects as discussed in Ref.[34]. 
In this work we limit our discussion to the interesting physics of the gauge vacuum sector, which is a good case to start - maybe also for experiments, which typically also would probably fix the gauge sector. Furthermore, as the referee pointed out above, there are some interesting connections to the quantum dimer models (a special case of two localized defects is actually shown in the paper in our discussion of the string tension).
Hence, the physics in different gauge sectors will be an interesting problem, but we believe that this goes beyond the scope of the present work.
\end{quote}


{\it
As the author mentioned, the effective Hamiltonian (2) indicates the
Haldane like phase.
The Haldane phase is a symmetry protected topological order.
In the Hamiltonian (2), which symmetry protects the numerical observed
Haldane-like phase?
It will be nice if the author can provide explicit symmetry analysis and tell
the readers which symmetry protects the Haldane-like phase in this model.
}

\begin{quote}
The quasi 1D toy model, resembles in some sense closely a traditional spin-1 chain. Indeed, as already suggested by the string- and parity-order we were discussing in the manuscript and the supplemental material, the protecting symmetries are similar; in this case the SPT-phase is protected by a $Z_2\times Z_2$ symmetry, $\pi$-rotations with the (generalized) $S_z$ and $S_x$ operators. We have added a discussion of the symmetries and a calculation of the generalized topological ``order-parameter'' for our trial state to the supplemental material.
\end{quote}

{\it
In the paragraph of large mass limit, line 6.
The expression of $S^z_k(r)$ contains a parameter $v$, which is not defined.
Is it a typo, or is it defined elsewhere?
}

\begin{quote}
It was a typo, thanks for the correction.
\end{quote}

{\it
 Not related to the criteria of publishing.
One question that I think is interesting for future study is: In the Hamiltonian
(1), the simulation is focusing on the half-filling case.
It will be interesting to know the result if we move away from the half-filling
limit.
}

\begin{quote}
We thank Referee B for the stimulating comment, hinting towards possible
further lines of investigation. 
\end{quote}


\end{document}

