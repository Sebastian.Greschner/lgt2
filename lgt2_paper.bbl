%merlin.mbs apsrev4-1.bst 2010-07-25 4.21a (PWD, AO, DPC) hacked
%Control: key (0)
%Control: author (8) initials jnrlst
%Control: editor formatted (1) identically to author
%Control: production of article title (-1) disabled
%Control: page (0) single
%Control: year (1) truncated
%Control: production of eprint (0) enabled
\begin{thebibliography}{48}%
\makeatletter
\providecommand \@ifxundefined [1]{%
 \@ifx{#1\undefined}
}%
\providecommand \@ifnum [1]{%
 \ifnum #1\expandafter \@firstoftwo
 \else \expandafter \@secondoftwo
 \fi
}%
\providecommand \@ifx [1]{%
 \ifx #1\expandafter \@firstoftwo
 \else \expandafter \@secondoftwo
 \fi
}%
\providecommand \natexlab [1]{#1}%
\providecommand \enquote  [1]{``#1''}%
\providecommand \bibnamefont  [1]{#1}%
\providecommand \bibfnamefont [1]{#1}%
\providecommand \citenamefont [1]{#1}%
\providecommand \href@noop [0]{\@secondoftwo}%
\providecommand \href [0]{\begingroup \@sanitize@url \@href}%
\providecommand \@href[1]{\@@startlink{#1}\@@href}%
\providecommand \@@href[1]{\endgroup#1\@@endlink}%
\providecommand \@sanitize@url [0]{\catcode `\\12\catcode `\$12\catcode
  `\&12\catcode `\#12\catcode `\^12\catcode `\_12\catcode `\%12\relax}%
\providecommand \@@startlink[1]{}%
\providecommand \@@endlink[0]{}%
\providecommand \url  [0]{\begingroup\@sanitize@url \@url }%
\providecommand \@url [1]{\endgroup\@href {#1}{\urlprefix }}%
\providecommand \urlprefix  [0]{URL }%
\providecommand \Eprint [0]{\href }%
\providecommand \doibase [0]{http://dx.doi.org/}%
\providecommand \selectlanguage [0]{\@gobble}%
\providecommand \bibinfo  [0]{\@secondoftwo}%
\providecommand \bibfield  [0]{\@secondoftwo}%
\providecommand \translation [1]{[#1]}%
\providecommand \BibitemOpen [0]{}%
\providecommand \bibitemStop [0]{}%
\providecommand \bibitemNoStop [0]{.\EOS\space}%
\providecommand \EOS [0]{\spacefactor3000\relax}%
\providecommand \BibitemShut  [1]{\csname bibitem#1\endcsname}%
\let\auto@bib@innerbib\@empty
%</preamble>
\bibitem [{\citenamefont {Wilson}(1974)}]{Wilson1974}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {K.~G.}\ \bibnamefont
  {Wilson}},\ }\href {\doibase 10.1103/PhysRevD.10.2445} {\bibfield  {journal}
  {\bibinfo  {journal} {Phys. Rev. D}\ }\textbf {\bibinfo {volume} {10}},\
  \bibinfo {pages} {2445} (\bibinfo {year} {1974})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Kogut}(1983)}]{Kogut1983}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {J.~B.}\ \bibnamefont
  {Kogut}},\ }\href@noop {} {\bibfield  {journal} {\bibinfo  {journal} {Rev.
  Mod. Phys.}\ }\textbf {\bibinfo {volume} {55}},\ \bibinfo {pages} {775}
  (\bibinfo {year} {1983})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Levin}\ and\ \citenamefont {Wen}(2005)}]{Levin2005}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {M.A.}~\bibnamefont
  {Levin}}\ and\ \bibinfo {author} {\bibfnamefont {X.-G.}\ \bibnamefont
  {Wen}},\ }\href@noop {} {\bibfield  {journal} {\bibinfo  {journal} {Rev. Mod.
  Phys.}\ }\textbf {\bibinfo {volume} {77}},\ \bibinfo {pages} {871} (\bibinfo
  {year} {2005})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Wiese}(2013)}]{Wiese2013}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {U.-J.}\ \bibnamefont
  {Wiese}},\ }\href@noop {} {\bibfield  {journal} {\bibinfo  {journal} {Annalen
  der Physik}\ }\textbf {\bibinfo {volume} {525}},\ \bibinfo {pages} {777}
  (\bibinfo {year} {2013})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Cirac}\ \emph {et~al.}(2010)\citenamefont {Cirac},
  \citenamefont {Maraner},\ and\ \citenamefont {Pachos}}]{Cirac2010}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {J.~I.}\ \bibnamefont
  {Cirac}}, \bibinfo {author} {\bibfnamefont {P.}~\bibnamefont {Maraner}}, \
  and\ \bibinfo {author} {\bibfnamefont {J.~K.}\ \bibnamefont {Pachos}},\
  }\href@noop {} {\bibfield  {journal} {\bibinfo  {journal} {Phys. Rev. Lett.}\
  }\textbf {\bibinfo {volume} {105}},\ \bibinfo {pages} {190403} (\bibinfo
  {year} {2010})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Zohar}\ and\ \citenamefont
  {Reznik}(2011)}]{Zohar2011}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {E.}~\bibnamefont
  {Zohar}}\ and\ \bibinfo {author} {\bibfnamefont {B.}~\bibnamefont {Reznik}},\
  }\href@noop {} {\bibfield  {journal} {\bibinfo  {journal} {Phys. Rev. Lett.}\
  }\textbf {\bibinfo {volume} {107}},\ \bibinfo {pages} {275301} (\bibinfo
  {year} {2011})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Kapit}\ and\ \citenamefont
  {Mueller}(2011)}]{Kapit2011}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {E.}~\bibnamefont
  {Kapit}}\ and\ \bibinfo {author} {\bibfnamefont {E.}~\bibnamefont
  {Mueller}},\ }\href@noop {} {\bibfield  {journal} {\bibinfo  {journal} {Phys.
  Rev. A}\ }\textbf {\bibinfo {volume} {83}},\ \bibinfo {pages} {033625}
  (\bibinfo {year} {2011})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Zohar}\ \emph {et~al.}(2012)\citenamefont {Zohar},
  \citenamefont {Cirac},\ and\ \citenamefont {Reznik}}]{Zohar2012}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {E.}~\bibnamefont
  {Zohar}}, \bibinfo {author} {\bibfnamefont {J.~I.}\ \bibnamefont {Cirac}}, \
  and\ \bibinfo {author} {\bibfnamefont {B.}~\bibnamefont {Reznik}},\
  }\href@noop {} {\bibfield  {journal} {\bibinfo  {journal} {Phys. Rev. Lett.}\
  }\textbf {\bibinfo {volume} {109}},\ \bibinfo {pages} {125302} (\bibinfo
  {year} {2012})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Banerjee}\ \emph {et~al.}(2012)\citenamefont
  {Banerjee}, \citenamefont {Dalmonte}, \citenamefont {M{\"u}ller},
  \citenamefont {Rico}, \citenamefont {Stebler}, \citenamefont {Wiese},\ and\
  \citenamefont {Zoller}}]{Banerjee2012}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {D.}~\bibnamefont
  {Banerjee}}, \bibinfo {author} {\bibfnamefont {M.}~\bibnamefont {Dalmonte}},
  \bibinfo {author} {\bibfnamefont {M.}~\bibnamefont {M{\"u}ller}}, \bibinfo
  {author} {\bibfnamefont {E.}~\bibnamefont {Rico}}, \bibinfo {author}
  {\bibfnamefont {P.}~\bibnamefont {Stebler}}, \bibinfo {author} {\bibfnamefont
  {U.-J.}\ \bibnamefont {Wiese}}, \ and\ \bibinfo {author} {\bibfnamefont
  {P.}~\bibnamefont {Zoller}},\ }\href@noop {} {\bibfield  {journal} {\bibinfo
  {journal} {Phys. Rev. Lett.}\ }\textbf {\bibinfo {volume} {109}},\ \bibinfo
  {pages} {175302} (\bibinfo {year} {2012})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Zohar}\ \emph
  {et~al.}(2013{\natexlab{a}})\citenamefont {Zohar}, \citenamefont {Cirac},\
  and\ \citenamefont {Reznik}}]{Zohar2013}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {E.}~\bibnamefont
  {Zohar}}, \bibinfo {author} {\bibfnamefont {J.~I.}\ \bibnamefont {Cirac}}, \
  and\ \bibinfo {author} {\bibfnamefont {B.}~\bibnamefont {Reznik}},\
  }\href@noop {} {\bibfield  {journal} {\bibinfo  {journal} {Phys. Rev. Lett.}\
  }\textbf {\bibinfo {volume} {110}},\ \bibinfo {pages} {055302} (\bibinfo
  {year} {2013}{\natexlab{a}})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Banerjee}\ \emph {et~al.}(2013)\citenamefont
  {Banerjee}, \citenamefont {B{\"o}gli}, \citenamefont {Dalmonte},
  \citenamefont {Rico}, \citenamefont {Stebler}, \citenamefont {Wiese},\ and\
  \citenamefont {Zoller}}]{Banerjee2013}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {D.}~\bibnamefont
  {Banerjee}}, \bibinfo {author} {\bibfnamefont {M.}~\bibnamefont {B{\"o}gli}},
  \bibinfo {author} {\bibfnamefont {M.}~\bibnamefont {Dalmonte}}, \bibinfo
  {author} {\bibfnamefont {E.}~\bibnamefont {Rico}}, \bibinfo {author}
  {\bibfnamefont {P.}~\bibnamefont {Stebler}}, \bibinfo {author} {\bibfnamefont
  {U.-J.}\ \bibnamefont {Wiese}}, \ and\ \bibinfo {author} {\bibfnamefont
  {P.}~\bibnamefont {Zoller}},\ }\href@noop {} {\bibfield  {journal} {\bibinfo
  {journal} {Phys. Rev. Lett.}\ }\textbf {\bibinfo {volume} {110}},\ \bibinfo
  {pages} {125303} (\bibinfo {year} {2013})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Zohar}\ \emph
  {et~al.}(2013{\natexlab{b}})\citenamefont {Zohar}, \citenamefont {Cirac},\
  and\ \citenamefont {Reznik}}]{Zohar2013b}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {E.}~\bibnamefont
  {Zohar}}, \bibinfo {author} {\bibfnamefont {J.~I.}\ \bibnamefont {Cirac}}, \
  and\ \bibinfo {author} {\bibfnamefont {B.}~\bibnamefont {Reznik}},\
  }\href@noop {} {\bibfield  {journal} {\bibinfo  {journal} {Phys. Rev. Lett.}\
  }\textbf {\bibinfo {volume} {110}},\ \bibinfo {pages} {125304} (\bibinfo
  {year} {2013}{\natexlab{b}})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Tagliacozzo}\ \emph {et~al.}(2013)\citenamefont
  {Tagliacozzo}, \citenamefont {Celi}, \citenamefont {Orland}, \citenamefont
  {Mitchell},\ and\ \citenamefont {Lewenstein}}]{Tagliacozzo2013}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {L.}~\bibnamefont
  {Tagliacozzo}}, \bibinfo {author} {\bibfnamefont {A.}~\bibnamefont {Celi}},
  \bibinfo {author} {\bibfnamefont {P.}~\bibnamefont {Orland}}, \bibinfo
  {author} {\bibfnamefont {M.}~\bibnamefont {Mitchell}}, \ and\ \bibinfo
  {author} {\bibfnamefont {M.}~\bibnamefont {Lewenstein}},\ }\href@noop {}
  {\bibfield  {journal} {\bibinfo  {journal} {Nat. Comm.}\ }\textbf {\bibinfo
  {volume} {4}} (\bibinfo {year} {2013})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Stannigel}\ \emph {et~al.}(2014)\citenamefont
  {Stannigel}, \citenamefont {Hauke}, \citenamefont {Marcos}, \citenamefont
  {Hafezi}, \citenamefont {Diehl}, \citenamefont {Dalmonte},\ and\
  \citenamefont {Zoller}}]{Stannigel2014}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {K.}~\bibnamefont
  {Stannigel}}, \bibinfo {author} {\bibfnamefont {P.}~\bibnamefont {Hauke}},
  \bibinfo {author} {\bibfnamefont {D.}~\bibnamefont {Marcos}}, \bibinfo
  {author} {\bibfnamefont {M.}~\bibnamefont {Hafezi}}, \bibinfo {author}
  {\bibfnamefont {S.}~\bibnamefont {Diehl}}, \bibinfo {author} {\bibfnamefont
  {M.}~\bibnamefont {Dalmonte}}, \ and\ \bibinfo {author} {\bibfnamefont
  {P.}~\bibnamefont {Zoller}},\ }\href {\doibase
  10.1103/PhysRevLett.112.120406} {\bibfield  {journal} {\bibinfo  {journal}
  {Phys. Rev. Lett.}\ }\textbf {\bibinfo {volume} {112}},\ \bibinfo {pages}
  {120406} (\bibinfo {year} {2014})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Kasper}\ \emph {et~al.}(2016)\citenamefont {Kasper},
  \citenamefont {Hebenstreit}, \citenamefont {Oberthaler},\ and\ \citenamefont
  {Berges}}]{Kasper2016}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {V.}~\bibnamefont
  {Kasper}}, \bibinfo {author} {\bibfnamefont {F.}~\bibnamefont {Hebenstreit}},
  \bibinfo {author} {\bibfnamefont {M.}~\bibnamefont {Oberthaler}}, \ and\
  \bibinfo {author} {\bibfnamefont {J.}~\bibnamefont {Berges}},\ }\href@noop {}
  {\bibfield  {journal} {\bibinfo  {journal} {Phys. Lett. B}\ }\textbf
  {\bibinfo {volume} {760}},\ \bibinfo {pages} {742} (\bibinfo {year}
  {2016})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Cardarelli}\ \emph {et~al.}(2017)\citenamefont
  {Cardarelli}, \citenamefont {Greschner},\ and\ \citenamefont
  {Santos}}]{Cardarelli2017}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {L.}~\bibnamefont
  {Cardarelli}}, \bibinfo {author} {\bibfnamefont {S.}~\bibnamefont
  {Greschner}}, \ and\ \bibinfo {author} {\bibfnamefont {L.}~\bibnamefont
  {Santos}},\ }\href {\doibase 10.1103/PhysRevLett.119.180402} {\bibfield
  {journal} {\bibinfo  {journal} {Phys. Rev. Lett.}\ }\textbf {\bibinfo
  {volume} {119}},\ \bibinfo {pages} {180402} (\bibinfo {year}
  {2017})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Zache}\ \emph {et~al.}(2018)\citenamefont {Zache},
  \citenamefont {Hebenstreit}, \citenamefont {Jendrzejewski}, \citenamefont
  {Oberthaler}, \citenamefont {Berges},\ and\ \citenamefont
  {Hauke}}]{Zache2018}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {T.~V.}\ \bibnamefont
  {Zache}}, \bibinfo {author} {\bibfnamefont {F.}~\bibnamefont {Hebenstreit}},
  \bibinfo {author} {\bibfnamefont {F.}~\bibnamefont {Jendrzejewski}}, \bibinfo
  {author} {\bibfnamefont {M.~K.}\ \bibnamefont {Oberthaler}}, \bibinfo
  {author} {\bibfnamefont {J.}~\bibnamefont {Berges}}, \ and\ \bibinfo {author}
  {\bibfnamefont {P.}~\bibnamefont {Hauke}},\ }\href {\doibase
  10.1088/2058-9565/aac33b} {\bibfield  {journal} {\bibinfo  {journal} {Quantum
  Science and Technology}\ }\textbf {\bibinfo {volume} {3}},\ \bibinfo {pages}
  {034010} (\bibinfo {year} {2018})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Barbiero}\ \emph {et~al.}(2019)\citenamefont
  {Barbiero}, \citenamefont {Schweizer}, \citenamefont {Aidelsburger},
  \citenamefont {Demler}, \citenamefont {Goldman},\ and\ \citenamefont
  {Grusdt}}]{barbiero2018}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {L.}~\bibnamefont
  {Barbiero}}, \bibinfo {author} {\bibfnamefont {C.}~\bibnamefont {Schweizer}},
  \bibinfo {author} {\bibfnamefont {M.}~\bibnamefont {Aidelsburger}}, \bibinfo
  {author} {\bibfnamefont {E.}~\bibnamefont {Demler}}, \bibinfo {author}
  {\bibfnamefont {N.}~\bibnamefont {Goldman}}, \ and\ \bibinfo {author}
  {\bibfnamefont {F.}~\bibnamefont {Grusdt}},\ }\href@noop {} {\bibfield
  {journal} {\bibinfo  {journal} {Science advances}\ }\textbf {\bibinfo
  {volume} {5}},\ \bibinfo {pages} {eaav7444} (\bibinfo {year}
  {2019})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Martinez}\ \emph {et~al.}(2016)\citenamefont
  {Martinez}, \citenamefont {Muschik}, \citenamefont {Schindler}, \citenamefont
  {Nigg}, \citenamefont {Erhard}, \citenamefont {Heyl}, \citenamefont {Hauke},
  \citenamefont {Dalmonte}, \citenamefont {Monz}, \citenamefont {Zoller},\ and\
  \citenamefont {Blatt}}]{Martinez2016}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {E.~A.}\ \bibnamefont
  {Martinez}}, \bibinfo {author} {\bibfnamefont {C.~A.}\ \bibnamefont
  {Muschik}}, \bibinfo {author} {\bibfnamefont {P.}~\bibnamefont {Schindler}},
  \bibinfo {author} {\bibfnamefont {D.}~\bibnamefont {Nigg}}, \bibinfo {author}
  {\bibfnamefont {A.}~\bibnamefont {Erhard}}, \bibinfo {author} {\bibfnamefont
  {M.}~\bibnamefont {Heyl}}, \bibinfo {author} {\bibfnamefont {P.}~\bibnamefont
  {Hauke}}, \bibinfo {author} {\bibfnamefont {M.}~\bibnamefont {Dalmonte}},
  \bibinfo {author} {\bibfnamefont {T.}~\bibnamefont {Monz}}, \bibinfo {author}
  {\bibfnamefont {P.}~\bibnamefont {Zoller}}, \ and\ \bibinfo {author}
  {\bibfnamefont {R.}~\bibnamefont {Blatt}},\ }\href
  {http://dx.doi.org/10.1038/nature18318} {\bibfield  {journal} {\bibinfo
  {journal} {Nature}\ }\textbf {\bibinfo {volume} {534}},\ \bibinfo {pages}
  {516} (\bibinfo {year} {2016})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Bernien}\ \emph {et~al.}(2017)\citenamefont
  {Bernien}, \citenamefont {Schwartz}, \citenamefont {Keesling}, \citenamefont
  {Levine}, \citenamefont {Omran}, \citenamefont {Pichler}, \citenamefont
  {Choi}, \citenamefont {Zibrov}, \citenamefont {Endres}, \citenamefont
  {Greiner}, \citenamefont {Vuletic},\ and\ \citenamefont
  {Lukin}}]{Bernien2017}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {H.}~\bibnamefont
  {Bernien}}, \bibinfo {author} {\bibfnamefont {S.}~\bibnamefont {Schwartz}},
  \bibinfo {author} {\bibfnamefont {A.}~\bibnamefont {Keesling}}, \bibinfo
  {author} {\bibfnamefont {H.}~\bibnamefont {Levine}}, \bibinfo {author}
  {\bibfnamefont {A.}~\bibnamefont {Omran}}, \bibinfo {author} {\bibfnamefont
  {H.}~\bibnamefont {Pichler}}, \bibinfo {author} {\bibfnamefont
  {S.}~\bibnamefont {Choi}}, \bibinfo {author} {\bibfnamefont {A.~S.}\
  \bibnamefont {Zibrov}}, \bibinfo {author} {\bibfnamefont {M.}~\bibnamefont
  {Endres}}, \bibinfo {author} {\bibfnamefont {M.}~\bibnamefont {Greiner}},
  \bibinfo {author} {\bibfnamefont {V.}~\bibnamefont {Vuletic}}, \ and\
  \bibinfo {author} {\bibfnamefont {M.~D.}\ \bibnamefont {Lukin}},\ }\href
  {https://doi.org/10.1038/nature24622} {\bibfield  {journal} {\bibinfo
  {journal} {Nature}\ }\textbf {\bibinfo {volume} {551}},\ \bibinfo {pages}
  {579 EP } (\bibinfo {year} {2017})},\ \bibinfo {note} {article}\BibitemShut
  {NoStop}%
\bibitem [{\citenamefont {Clark}\ \emph {et~al.}(2018)\citenamefont {Clark},
  \citenamefont {Anderson}, \citenamefont {Feng}, \citenamefont {Gaj},
  \citenamefont {Levin},\ and\ \citenamefont {Chin}}]{Clark2018}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {L.~W.}\ \bibnamefont
  {Clark}}, \bibinfo {author} {\bibfnamefont {B.~M.}\ \bibnamefont {Anderson}},
  \bibinfo {author} {\bibfnamefont {L.}~\bibnamefont {Feng}}, \bibinfo {author}
  {\bibfnamefont {A.}~\bibnamefont {Gaj}}, \bibinfo {author} {\bibfnamefont
  {K.}~\bibnamefont {Levin}}, \ and\ \bibinfo {author} {\bibfnamefont
  {C.}~\bibnamefont {Chin}},\ }\href@noop {} {\bibfield  {journal} {\bibinfo
  {journal} {Phys. Rev. Lett.}\ }\textbf {\bibinfo {volume} {121}},\ \bibinfo
  {pages} {030402} (\bibinfo {year} {2018})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {G{\"o}rg}\ \emph {et~al.}(2019)\citenamefont
  {G{\"o}rg}, \citenamefont {Sandholzer}, \citenamefont {Minguzzi},
  \citenamefont {Desbuquois}, \citenamefont {Messer},\ and\ \citenamefont
  {Esslinger}}]{Goerg2019}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {F.}~\bibnamefont
  {G{\"o}rg}}, \bibinfo {author} {\bibfnamefont {K.}~\bibnamefont
  {Sandholzer}}, \bibinfo {author} {\bibfnamefont {J.}~\bibnamefont
  {Minguzzi}}, \bibinfo {author} {\bibfnamefont {R.}~\bibnamefont
  {Desbuquois}}, \bibinfo {author} {\bibfnamefont {M.}~\bibnamefont {Messer}},
  \ and\ \bibinfo {author} {\bibfnamefont {T.}~\bibnamefont {Esslinger}},\
  }\href@noop {} {\bibfield  {journal} {\bibinfo  {journal} {Nature Physics}\
  }\textbf {\bibinfo {volume} {15}},\ \bibinfo {pages} {1161} (\bibinfo {year}
  {2019})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Schweizer}\ \emph {et~al.}(2019)\citenamefont
  {Schweizer}, \citenamefont {Grusdt}, \citenamefont {Berngruber},
  \citenamefont {Barbiero}, \citenamefont {Demler}, \citenamefont {Goldman},
  \citenamefont {Bloch},\ and\ \citenamefont {Aidelsburger}}]{schweizer2019}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {C.}~\bibnamefont
  {Schweizer}}, \bibinfo {author} {\bibfnamefont {F.}~\bibnamefont {Grusdt}},
  \bibinfo {author} {\bibfnamefont {M.}~\bibnamefont {Berngruber}}, \bibinfo
  {author} {\bibfnamefont {L.}~\bibnamefont {Barbiero}}, \bibinfo {author}
  {\bibfnamefont {E.}~\bibnamefont {Demler}}, \bibinfo {author} {\bibfnamefont
  {N.}~\bibnamefont {Goldman}}, \bibinfo {author} {\bibfnamefont
  {I.}~\bibnamefont {Bloch}}, \ and\ \bibinfo {author} {\bibfnamefont
  {M.}~\bibnamefont {Aidelsburger}},\ }\href@noop {} {\bibfield  {journal}
  {\bibinfo  {journal} {Nature Physics}\ }\textbf {\bibinfo {volume} {15}},\
  \bibinfo {pages} {1168} (\bibinfo {year} {2019})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Mil}\ \emph {et~al.}(2019)\citenamefont {Mil},
  \citenamefont {Zache}, \citenamefont {Hegde}, \citenamefont {Xia},
  \citenamefont {Bhatt}, \citenamefont {Oberthaler}, \citenamefont {Hauke},
  \citenamefont {Berges},\ and\ \citenamefont {Jendrzejewski}}]{Mil2019}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {A.}~\bibnamefont
  {Mil}}, \bibinfo {author} {\bibfnamefont {T.~V.}\ \bibnamefont {Zache}},
  \bibinfo {author} {\bibfnamefont {A.}~\bibnamefont {Hegde}}, \bibinfo
  {author} {\bibfnamefont {A.}~\bibnamefont {Xia}}, \bibinfo {author}
  {\bibfnamefont {R.~P.}\ \bibnamefont {Bhatt}}, \bibinfo {author}
  {\bibfnamefont {M.~K.}\ \bibnamefont {Oberthaler}}, \bibinfo {author}
  {\bibfnamefont {P.}~\bibnamefont {Hauke}}, \bibinfo {author} {\bibfnamefont
  {J.}~\bibnamefont {Berges}}, \ and\ \bibinfo {author} {\bibfnamefont
  {F.}~\bibnamefont {Jendrzejewski}},\ }\href@noop {} {\bibfield  {journal}
  {\bibinfo  {journal} {arXiv preprint arXiv:1909.07641}\ } (\bibinfo {year}
  {2019})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Chandrasekharan}\ and\ \citenamefont
  {Wiese}(1997)}]{Chandrasekharan1997}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {S.}~\bibnamefont
  {Chandrasekharan}}\ and\ \bibinfo {author} {\bibfnamefont {U.-J.}\
  \bibnamefont {Wiese}},\ }\href {\doibase
  http://dx.doi.org/10.1016/S0550-3213(97)80041-7} {\bibfield  {journal}
  {\bibinfo  {journal} {Nucl. Phys. B}\ }\textbf {\bibinfo {volume} {492}},\
  \bibinfo {pages} {455 } (\bibinfo {year} {1997})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Dai}\ \emph {et~al.}(2017)\citenamefont {Dai},
  \citenamefont {Yang}, \citenamefont {Reingruber}, \citenamefont {Sun},
  \citenamefont {Xu}, \citenamefont {Chen}, \citenamefont {Yuan},\ and\
  \citenamefont {Pan}}]{dai2017four}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {H.-N.}\ \bibnamefont
  {Dai}}, \bibinfo {author} {\bibfnamefont {B.}~\bibnamefont {Yang}}, \bibinfo
  {author} {\bibfnamefont {A.}~\bibnamefont {Reingruber}}, \bibinfo {author}
  {\bibfnamefont {H.}~\bibnamefont {Sun}}, \bibinfo {author} {\bibfnamefont
  {X.-F.}\ \bibnamefont {Xu}}, \bibinfo {author} {\bibfnamefont {Y.-A.}\
  \bibnamefont {Chen}}, \bibinfo {author} {\bibfnamefont {Z.-S.}\ \bibnamefont
  {Yuan}}, \ and\ \bibinfo {author} {\bibfnamefont {J.-W.}\ \bibnamefont
  {Pan}},\ }\href@noop {} {\bibfield  {journal} {\bibinfo  {journal} {Nat.
  Phys.}\ }\textbf {\bibinfo {volume} {13}},\ \bibinfo {pages} {1195} (\bibinfo
  {year} {2017})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Celi}\ \emph {et~al.}(2019)\citenamefont {Celi},
  \citenamefont {Vermersch}, \citenamefont {Viyuela}, \citenamefont {Pichler},
  \citenamefont {Lukin},\ and\ \citenamefont {Zoller}}]{Celi2019}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {A.}~\bibnamefont
  {Celi}}, \bibinfo {author} {\bibfnamefont {B.}~\bibnamefont {Vermersch}},
  \bibinfo {author} {\bibfnamefont {O.}~\bibnamefont {Viyuela}}, \bibinfo
  {author} {\bibfnamefont {H.}~\bibnamefont {Pichler}}, \bibinfo {author}
  {\bibfnamefont {M.~D.}\ \bibnamefont {Lukin}}, \ and\ \bibinfo {author}
  {\bibfnamefont {P.}~\bibnamefont {Zoller}},\ }\href@noop {} {\bibfield
  {journal} {\bibinfo  {journal} {arXiv preprint arXiv:1907.03311}\ } (\bibinfo
  {year} {2019})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Bohrdt}\ \emph {et~al.}(2019)\citenamefont {Bohrdt},
  \citenamefont {Omran}, \citenamefont {Demler}, \citenamefont {Gazit},\ and\
  \citenamefont {Grusdt}}]{Bohrdt2019}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {A.}~\bibnamefont
  {Bohrdt}}, \bibinfo {author} {\bibfnamefont {A.}~\bibnamefont {Omran}},
  \bibinfo {author} {\bibfnamefont {E.}~\bibnamefont {Demler}}, \bibinfo
  {author} {\bibfnamefont {S.}~\bibnamefont {Gazit}}, \ and\ \bibinfo {author}
  {\bibfnamefont {F.}~\bibnamefont {Grusdt}},\ }\href@noop {} {\bibfield
  {journal} {\bibinfo  {journal} {arXiv preprint arXiv:1910.00023}\ } (\bibinfo
  {year} {2019})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Rokhsar}\ and\ \citenamefont
  {Kivelson}(1988)}]{rokhsar1988}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {D.~S.}\ \bibnamefont
  {Rokhsar}}\ and\ \bibinfo {author} {\bibfnamefont {S.~A.}\ \bibnamefont
  {Kivelson}},\ }\href@noop {} {\bibfield  {journal} {\bibinfo  {journal}
  {Phys. Rev. Lett.}\ }\textbf {\bibinfo {volume} {61}},\ \bibinfo {pages}
  {2376} (\bibinfo {year} {1988})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Shastry}\ and\ \citenamefont
  {Sutherland}(1981)}]{Shastry1981}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {B.~S.}\ \bibnamefont
  {Shastry}}\ and\ \bibinfo {author} {\bibfnamefont {B.}~\bibnamefont
  {Sutherland}},\ }\href@noop {} {\bibfield  {journal} {\bibinfo  {journal}
  {Physica B+C}\ }\textbf {\bibinfo {volume} {108}},\ \bibinfo {pages} {1069}
  (\bibinfo {year} {1981})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Anderson}(1987)}]{Anderson1987}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {P.~W.}\ \bibnamefont
  {Anderson}},\ }\href {\doibase 10.1126/science.235.4793.1196} {\bibfield
  {journal} {\bibinfo  {journal} {Science}\ }\textbf {\bibinfo {volume}
  {235}},\ \bibinfo {pages} {1196} (\bibinfo {year} {1987})}\BibitemShut
  {NoStop}%
\bibitem [{\citenamefont {Moessner}\ and\ \citenamefont
  {Sondhi}(2002)}]{Moessner2002}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {R.}~\bibnamefont
  {Moessner}}\ and\ \bibinfo {author} {\bibfnamefont {S.~L.}\ \bibnamefont
  {Sondhi}},\ }\href {\doibase 10.1143/PTPS.145.37} {\bibfield  {journal}
  {\bibinfo  {journal} {Progress of Theoretical Physics Supplement}\ }\textbf
  {\bibinfo {volume} {145}},\ \bibinfo {pages} {37} (\bibinfo {year}
  {2002})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Ralko}\ \emph {et~al.}(2005)\citenamefont {Ralko},
  \citenamefont {Ferrero}, \citenamefont {Becca}, \citenamefont {Ivanov},\ and\
  \citenamefont {Mila}}]{Ralko2005}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {A.}~\bibnamefont
  {Ralko}}, \bibinfo {author} {\bibfnamefont {M.}~\bibnamefont {Ferrero}},
  \bibinfo {author} {\bibfnamefont {F.}~\bibnamefont {Becca}}, \bibinfo
  {author} {\bibfnamefont {D.}~\bibnamefont {Ivanov}}, \ and\ \bibinfo {author}
  {\bibfnamefont {F.}~\bibnamefont {Mila}},\ }\href {\doibase
  10.1103/PhysRevB.71.224109} {\bibfield  {journal} {\bibinfo  {journal} {Phys.
  Rev. B}\ }\textbf {\bibinfo {volume} {71}},\ \bibinfo {pages} {224109}
  (\bibinfo {year} {2005})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Brenes}\ \emph {et~al.}(2018)\citenamefont {Brenes},
  \citenamefont {Dalmonte}, \citenamefont {Heyl},\ and\ \citenamefont
  {Scardicchio}}]{Brenes2018}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {M.}~\bibnamefont
  {Brenes}}, \bibinfo {author} {\bibfnamefont {M.}~\bibnamefont {Dalmonte}},
  \bibinfo {author} {\bibfnamefont {M.}~\bibnamefont {Heyl}}, \ and\ \bibinfo
  {author} {\bibfnamefont {A.}~\bibnamefont {Scardicchio}},\ }\href {\doibase
  10.1103/PhysRevLett.120.030601} {\bibfield  {journal} {\bibinfo  {journal}
  {Phys. Rev. Lett.}\ }\textbf {\bibinfo {volume} {120}},\ \bibinfo {pages}
  {030601} (\bibinfo {year} {2018})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Feldmeier}\ \emph {et~al.}(2019)\citenamefont
  {Feldmeier}, \citenamefont {Pollmann},\ and\ \citenamefont
  {Knap}}]{feldmeier2019emergent}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {J.}~\bibnamefont
  {Feldmeier}}, \bibinfo {author} {\bibfnamefont {F.}~\bibnamefont {Pollmann}},
  \ and\ \bibinfo {author} {\bibfnamefont {M.}~\bibnamefont {Knap}},\
  }\href@noop {} {\bibfield  {journal} {\bibinfo  {journal} {Phys. Rev. Lett.}\
  }\textbf {\bibinfo {volume} {123}},\ \bibinfo {pages} {040601} (\bibinfo
  {year} {2019})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Turner}\ \emph {et~al.}(2018)\citenamefont {Turner},
  \citenamefont {Michailidis}, \citenamefont {Abanin}, \citenamefont {Serbyn},\
  and\ \citenamefont {Papi{\'c}}}]{turner2018scars}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {C.~J.}\ \bibnamefont
  {Turner}}, \bibinfo {author} {\bibfnamefont {A.~A.}\ \bibnamefont
  {Michailidis}}, \bibinfo {author} {\bibfnamefont {D.~A.}\ \bibnamefont
  {Abanin}}, \bibinfo {author} {\bibfnamefont {M.}~\bibnamefont {Serbyn}}, \
  and\ \bibinfo {author} {\bibfnamefont {Z.}~\bibnamefont {Papi{\'c}}},\
  }\href@noop {} {\bibfield  {journal} {\bibinfo  {journal} {Nat. Phys.}\
  }\textbf {\bibinfo {volume} {14}},\ \bibinfo {pages} {745} (\bibinfo {year}
  {2018})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Wirth}\ \emph {et~al.}(2011)\citenamefont {Wirth},
  \citenamefont {{\"O}lschl{\"a}ger},\ and\ \citenamefont
  {Hemmerich}}]{Wirth2011}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {G.}~\bibnamefont
  {Wirth}}, \bibinfo {author} {\bibfnamefont {M.}~\bibnamefont
  {{\"O}lschl{\"a}ger}}, \ and\ \bibinfo {author} {\bibfnamefont
  {A.}~\bibnamefont {Hemmerich}},\ }\href@noop {} {\bibfield  {journal}
  {\bibinfo  {journal} {Nature Phys.}\ }\textbf {\bibinfo {volume} {7}},\
  \bibinfo {pages} {147} (\bibinfo {year} {2011})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Schollw{\"o}ck}(2011)}]{Schollwock2011}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {U.}~\bibnamefont
  {Schollw{\"o}ck}},\ }\href@noop {} {\bibfield  {journal} {\bibinfo  {journal}
  {Annals of Physics}\ }\textbf {\bibinfo {volume} {326}},\ \bibinfo {pages}
  {96} (\bibinfo {year} {2011})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Dalmonte}\ and\ \citenamefont
  {Montangero}(2016)}]{Dalmonte2016cont}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {M.}~\bibnamefont
  {Dalmonte}}\ and\ \bibinfo {author} {\bibfnamefont {S.}~\bibnamefont
  {Montangero}},\ }\href {\doibase 10.1080/00107514.2016.1151199} {\bibfield
  {journal} {\bibinfo  {journal} {Contemporary Physics}\ }\textbf {\bibinfo
  {volume} {57}},\ \bibinfo {pages} {388} (\bibinfo {year} {2016})}\BibitemShut
  {NoStop}%
\bibitem [{\citenamefont {Tschirsich}\ \emph {et~al.}(2019)\citenamefont
  {Tschirsich}, \citenamefont {Montangero},\ and\ \citenamefont
  {Dalmonte}}]{Tschirsich2019}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {F.}~\bibnamefont
  {Tschirsich}}, \bibinfo {author} {\bibfnamefont {S.}~\bibnamefont
  {Montangero}}, \ and\ \bibinfo {author} {\bibfnamefont {M.}~\bibnamefont
  {Dalmonte}},\ }\href {\doibase 10.21468/SciPostPhys.6.3.028} {\bibfield
  {journal} {\bibinfo  {journal} {SciPost Phys.}\ }\textbf {\bibinfo {volume}
  {6}},\ \bibinfo {pages} {28} (\bibinfo {year} {2019})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Chepiga}\ and\ \citenamefont
  {Mila}(2019)}]{Chepiga2019dmrg}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {N.}~\bibnamefont
  {Chepiga}}\ and\ \bibinfo {author} {\bibfnamefont {F.}~\bibnamefont {Mila}},\
  }\href@noop {} {\bibfield  {journal} {\bibinfo  {journal} {SciPost Physics}\
  }\textbf {\bibinfo {volume} {6}},\ \bibinfo {pages} {033} (\bibinfo {year}
  {2019})}\BibitemShut {NoStop}%
\bibitem [{DMR()}]{DMRGnote}%
  \BibitemOpen
  \href@noop {} {\emph {\bibinfo {title} {{\rm We keep up to $300$ DMRG states
  which shows sufficient convergence for the gapped phases described in the
  paper.}}}}\BibitemShut {Stop}%
\bibitem [{Arr()}]{Arrows}%
  \BibitemOpen
  \href@noop {} {\emph {\bibinfo {title} {{\rm Following the standard
  convention, we depict in our figures the spin $S^z$ as
  $\uparrow$~($\rightarrow$) and $\downarrow$~($\leftarrow$) in
  $y$~($x$)-links. A vortex~(antivortex) plaquette has a spin configuration up,
  up, down, down~(down, down, up, up) starting with the spin at the left side
  and proceeding clockwise.}}}}\BibitemShut {Stop}%
\bibitem [{sup()}]{supmat}%
  \BibitemOpen
  \href@noop {} {\emph {\bibinfo {title} {{\rm See {Supplementary Material},
  which includes Refs.~\cite{aklt, Pollmann2010, Pollmann2012, Pollmann2012A},
  for detailed numerical results and extended data on the RK-states and
  string-tension calculations, and on the simplified 1D model of Eq.
  (2).}}}}\BibitemShut {Stop}%
\bibitem [{\citenamefont {Affleck}\ \emph {et~al.}(1987)\citenamefont
  {Affleck}, \citenamefont {Kennedy}, \citenamefont {Lieb},\ and\ \citenamefont
  {Tasaki}}]{aklt}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {I.}~\bibnamefont
  {Affleck}}, \bibinfo {author} {\bibfnamefont {T.}~\bibnamefont {Kennedy}},
  \bibinfo {author} {\bibfnamefont {E.H.}~\bibnamefont {Lieb}}, \ and\ \bibinfo
  {author} {\bibfnamefont {H.}~\bibnamefont {Tasaki}},\ }\href@noop {}
  {\bibfield  {journal} {\bibinfo  {journal} {Phys. Rev. Lett.}\ }\textbf
  {\bibinfo {volume} {59}},\ \bibinfo {pages} {799} (\bibinfo {year}
  {1987})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Pollmann}\ \emph {et~al.}(2010)\citenamefont
  {Pollmann}, \citenamefont {Turner}, \citenamefont {Berg},\ and\ \citenamefont
  {Oshikawa}}]{Pollmann2010}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {F.}~\bibnamefont
  {Pollmann}}, \bibinfo {author} {\bibfnamefont {A.~M.}\ \bibnamefont
  {Turner}}, \bibinfo {author} {\bibfnamefont {E.}~\bibnamefont {Berg}}, \ and\
  \bibinfo {author} {\bibfnamefont {M.}~\bibnamefont {Oshikawa}},\ }\href@noop
  {} {\bibfield  {journal} {\bibinfo  {journal} {Phys. Rev. B}\ }\textbf
  {\bibinfo {volume} {81}},\ \bibinfo {pages} {064439} (\bibinfo {year}
  {2010})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Pollmann}\ and\ \citenamefont
  {Turner}(2012)}]{Pollmann2012}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {F.}~\bibnamefont
  {Pollmann}}\ and\ \bibinfo {author} {\bibfnamefont {A.~M.}\ \bibnamefont
  {Turner}},\ }\href@noop {} {\bibfield  {journal} {\bibinfo  {journal} {Phys.
  Rev. B}\ }\textbf {\bibinfo {volume} {86}},\ \bibinfo {pages} {125441}
  (\bibinfo {year} {2012})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Pollmann}\ \emph {et~al.}(2012)\citenamefont
  {Pollmann}, \citenamefont {Berg}, \citenamefont {Turner},\ and\ \citenamefont
  {Oshikawa}}]{Pollmann2012A}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {F.}~\bibnamefont
  {Pollmann}}, \bibinfo {author} {\bibfnamefont {E.}~\bibnamefont {Berg}},
  \bibinfo {author} {\bibfnamefont {A.~M.}\ \bibnamefont {Turner}}, \ and\
  \bibinfo {author} {\bibfnamefont {M.}~\bibnamefont {Oshikawa}},\ }\href@noop
  {} {\bibfield  {journal} {\bibinfo  {journal} {Phys. Rev. B}\ }\textbf
  {\bibinfo {volume} {85}},\ \bibinfo {pages} {075125} (\bibinfo {year}
  {2012})}\BibitemShut {NoStop}%
\end{thebibliography}%
