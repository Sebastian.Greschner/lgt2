\documentclass[aps,pra,reprint,twocolumn,superscriptaddress,amsmath,amssymb,citeautoscript]{revtex4-1}

\usepackage{graphicx}
\usepackage{bm,amsmath,amssymb}
\usepackage{mathrsfs}
\usepackage{cases}
\usepackage{color}
 \usepackage{indentfirst}
\usepackage{epstopdf}
\usepackage{grffile}
\usepackage{xfrac}    % Works better with other fonts

\usepackage{adjustbox}

\renewcommand*{\thefigure}{S\arabic{figure}}


\DeclareGraphicsExtensions{.eps}
\newcommand{\sebastian}[1]{\textcolor{magenta}{[SG: #1]}}

\newcommand{\be}{\begin{equation}}
\newcommand{\ee}{\end{equation}}
\newcommand{\bea}{\begin{eqnarray}}
\newcommand{\eea}{\end{eqnarray}}
\newcommand{\la}{\langle}
\newcommand{\ra}{\rangle}
\newcommand{\ii}{\rm i}
\newcommand{\e}{\rm e}
\newcommand{\tr}{\rm tr}

\newcommand{\PO}{\mathcal{O}_{P}^2}
\newcommand{\SO}{\mathcal{O}_{S}^2}

\newcommand{\aU}{{\uparrow}}
\newcommand{\aL}{{\leftarrow}}
\newcommand{\aR}{{\rightarrow}}
\newcommand{\aD}{{\downarrow}}
\newcommand{\aH}{{\circ}}
\newcommand{\aP}{{\bullet}}

\newcommand{\eqQLM}{{(1)}}
\newcommand{\eqQLMoneD}{{(2)}}

\renewcommand\vec{\mathbf}

\newcommand{\vR}{\vec{r}}
\newcommand{\vRp}{\vec{r'}}
\newcommand{\vX}{\vec{x}}
\newcommand{\vY}{\vec{y}}


\newcommand{\CState}[4]{\arraycolsep=0.5pt\def\arraystretch{0.6}\scriptsize
\begin{array}{ccc}
{#1} & {#2} & {#4} \\
 & {#3} &
\end{array}}

\newcommand{\curleq}[2]{%
\begin{array}{#1}#2\end{array}}


\newcommand{\hannover}{Institut f\"ur Theoretische Physik, Leibniz Universit\"at Hannover, Appelstr. 2, DE-30167 Hannover, Germany}


\begin{document}
\title{Supplementary material to "Deconfining disordered phase in two-dimensional quantum link models"}

\author{Lorenzo Cardarelli} 
\affiliation{\hannover}
\author{Sebastian Greschner} 
\affiliation{Department of Quantum Matter Physics, University of Geneva, CH-1211 Geneva, Switzerland} 
\author{Luis Santos}
\affiliation{\hannover} 

\date{\today}
\begin{abstract}
In this supplementary material we give details on our numerical results for the simplified 1D model, two-, three- and four-leg ladders. We also show additional data for the classical RK-states and the calculation of the string tension.
\end{abstract}
\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Relation to quantum dimer models}

For the limit of  $|\mu| \gg J_x, J_y$, $\mu>0$, charges are pinned to the B-sublattice and the dynamics is reduced to the spins on the links. Indeed, this limit corresponds to a model of tightly packed hard-core dimers, a quantum dimer model on a square lattice. The Hamiltonian in second order perturbation theory is given by (with operators as defined in the main text)
\begin{align}
H_{\mu \gg J_x, J_y} = \sum_\vR \frac{J_\vec{k}^2}{\mu} S^z_{\vec{k}}(\vR)  \,+\, \cdots
\end{align}
The ellipsis contains ring-exchange terms from fourth order perturbation $\sim J_{x}^2 J_{y}^2/\mu^3$.  The physics for this strong-coupling dimer limit is indeed different from the phase diagram of Refs.~\cite{Ralko2005}. 
Traditionally, as in Refs.~\cite{Shastry1981,Anderson1987, Moessner2002, Ralko2005} the quantum dimer-model Hamiltonian is discussed with plaquette operators
\begin{align}
H_{QDM} = \sum_\vR (R^+_\vR + R^-_\vR) + \lambda Q_{\vR}^2 \,.
\end{align} 
On the square lattice this models have been shown to exhibit several phase transitions as a function of $\lambda$: Phases found and discussed e.g. in Ref.~\cite{Tschirsich2019} are Neel, columnar, or a plaquette-ordered (also called RVB-solid) phase. 
At the phase transition-point between columnar and plaquette-ordered phase, lies the so called Rokhsar-Kivelson point, which is discussed in the main text.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Simplified 1D model}

In the following, we present some details on the simplified 1D model described in Eq.~\eqQLMoneD{} of the main text. As discussed in the main text, the model can be described in a basis of six local states:
\begin{align*}
\left|0_\pm \right\rangle &= ( \left| \CState{\aL}{\aH}{\aU}{\aL} \right\rangle \pm \left| \CState{\aL}{\aP}{\aD}{\aL} \right\rangle ) /\sqrt2  \,, \\
\left|\tilde{0}_\pm\right\rangle &= ( \left| \CState{\aR}{\aH}{\aU}{\aR} \right\rangle \pm \left| \CState{\aR}{\aP}{\aD}{\aR} \right\rangle) /\sqrt2  \,, \\
\left|\alpha \right\rangle &= \left| \CState{\aR}{\aH}{\aD}{\aL} \right\rangle \,, \\
\left|\beta \right\rangle &= \left| \CState{\aL}{\aP}{\aU}{\aR} \right\rangle
\end{align*}
with the restrictions on the allowed sequence of these local states by Gauss' law: $0$ may be followed at its right by $0$ or $\beta$~($0\to 0,\beta$); $\tilde{0} \to \tilde{0}, \alpha$; $\alpha \to \beta, 0$; and 
$\beta \to \alpha, \tilde{0}$~(we remove the $\pm$ index).
As mentioned in the main text the ground states can be understood from a $J_y\ll J_x$ and $J_y\gg J_x$ limit as $\cdots |\alpha\ra |\beta\ra |\alpha\ra |\beta\ra \cdots$ and $\cdots |0_-\ra |0_-\ra \cdots$ or $\cdots |\tilde{0}_-\ra |\tilde{0}_-\ra \cdots$ crystalline states respectively.

%%
\begin{figure}[tb]
\includegraphics[width=\linewidth]{supmat_fig1_test_1d_m0.pdf}
\caption{Ground-state energy per site $E / L$ and average entanglement entropy $S_{vN}$ of the simplified 1D model~(infinite DMRG simulation, $\chi=80$). The lighter colored curves depict the $\chi=2$ ansatz described in the main text and Eq.~\eqref{eq:lgt1dMPS}.}
\label{fig:S_1d_m0}
\end{figure}
%%


%%
\begin{figure*}[tb]
%
\begin{minipage}[t]{.03\linewidth}
\raisebox{3.8cm}[0cm][0cm]{(a)}
\end{minipage}
\begin{minipage}[t]{.26\linewidth}
	\includegraphics[height=4cm]{supmat_fig2a_entspec_2L_OBC_m0.pdf}
\end{minipage}
%
\begin{minipage}[t]{.03\linewidth}
\raisebox{3.8cm}[0cm][0cm]{(b)}
\end{minipage}
\begin{minipage}[t]{.26\linewidth}
	\includegraphics[height=4cm]{supmat_fig2b_entspec_3L_OBC_m0_nolabel.pdf}
\end{minipage}
%
\begin{minipage}[t]{.03\linewidth}
\raisebox{3.8cm}[0cm][0cm]{(c)}
\end{minipage}
\begin{minipage}[t]{.26\linewidth}
	\includegraphics[height=4cm]{supmat_fig2c_entspec_4L_OBC_m0_nolabel.pdf}
\end{minipage}
%
\caption{Mid-system entanglement spectrum of the QLM Eq.~(1) of the main text at $\mu=0$ for (a) two-leg ladder, $L=200$ (b) three-leg ladder, $L=100$ (c) four-leg ladder, $L=100$ (DMRG data).}
\label{fig:S_entanglement}
\end{figure*}
%%

To shed light into the properties of the intermediate Haldane-like phase, we define the variational MPS ground state as $|\Psi\rangle = \sum_\sigma \Lambda \Gamma_\sigma | \sigma \rangle$. The $\Gamma_\sigma$ matrices, considered as an automorphism of $\chi\times\chi$ matrices, fulfill the following simple algebraic relations defined by Gauss' law.
A simple example is given by the following matrices, with the lowest non-trivial bond dimension $\chi=2$:
\begin{align}
\Gamma_\alpha &=  \sqrt{2} \left(
\begin{array}{cc} 
0 & 0 \\
\sin\phi & 0 
\end{array}\right) \nonumber\\
\Gamma_\beta &= \Gamma_A^T \nonumber\\
\Gamma_{\tilde{0}_\pm} &= \sqrt{2} \left(
\begin{array}{cc} 
\cos\phi & 0 \nonumber\\
0 & 0 
\end{array}\right) \\
\Gamma_{0_\pm} &= \sqrt{2} \left(
\begin{array}{cc} 
0 & 0 \\
0 & \cos\phi 
\end{array}\right)
\label{eq:lgt1dMPS}
\end{align}
and $\Lambda=\{ \frac{1}{\sqrt{2}}, \frac{1}{\sqrt{2}} \}$ which resembles an AKLT-model like state~\cite{aklt}. This ansatz has minimal energy for $\phi_{min}=\arctan\left(\frac{2J_x-J_y}{2J_x + J_y}\right)$. With this $E_{min}=-\frac{(2J_x + J_y)^2}{4J_x}$, which gives a reasonably good approximation of the actual ground-state energy in this regime obtained by a DMRG simulation, as shown in Fig.~\ref{fig:S_1d_m0}. Also the entanglement entropy of $\log(2)$ describes already well the actual values obtained by DMRG simulations with a higher bond-dimension~(see Fig.~\ref{fig:S_1d_m0}). 
The string order is given by $O_{SO} = \lim_{|x-x'|\to\infty} \la S^z_x \e^{\ii \pi \sum_{x<k<{x'}} S^z_k} S^z_{x'} \ra = (-1)^{i-j} \frac{\cos^4\phi}{4}$, while the parity order $O_{PO} = \lim_{|x-x'|\to\infty} \la \e^{\ii \pi \sum_{x<k<{x'}} S^z_k} \ra$ is exponentially suppressed.

As the Haldane-phase in this toy model, the SPT-phase is protected by a $Z_2 \times Z_2$ symmetry, with two $\pi$-rotations in the pseudo-spin space $\e^{\pi S_z}$ and $\e^{\pi S_x}$. Here, $S_z$ is defined as above and a corresponding $\e^{\pi S_x}$ maps the states $\alpha\to\beta$ and $0_\pm\to \tilde{0}_\pm$. For the trial-state of Eq.~\eqref{eq:lgt1dMPS} we may now define, following the discussion of Pollmann and Turner in Ref.~\cite{pollmann2012symmetry}, a generalized order parameter $\mathcal{O}_{Z_2\times Z_2} = \tr\left(U_x U_z U_x^\dagger U_z^\dagger \right) / 2$. The 2-dimensional representations of the symmetry group corresponding to the MPS ansatz $U_x$ and $U_z$ are derived from a generalized transfer matrix and are given by by the actual Pauli-matrices $\sigma_z$ and $\sigma_x$. With this we find $\mathcal{O}_{Z_2\times Z_2}=-1$ indicating the topologically non-trivial character of the phase.

%%
\begin{figure}[tb]
%
\begin{minipage}[t]{.05\linewidth}
\raisebox{3.1cm}[0cm][0cm]{(a)}
\end{minipage}
\begin{minipage}[t]{.7\linewidth}
	\includegraphics[width=.99\linewidth]{supmat_fig3a_parity_string_order_2L_OBC_m0_leg0_nolabel.pdf}
	\vspace{1pt}
\end{minipage}

%
\begin{minipage}[t]{.05\linewidth}
\raisebox{3.1cm}[0cm][0cm]{(b)}
\end{minipage}
\begin{minipage}[t]{.7\linewidth}
	\includegraphics[width=.99\linewidth]{supmat_fig3b_parity_string_order_3L_OBC_m0_leg0_nolabel.pdf}
	\vspace{1pt}
\end{minipage}
%

\begin{minipage}[t]{.05\linewidth}
\raisebox{3.1cm}[0cm][0cm]{(c)}
\end{minipage}
\begin{minipage}[t]{.7\linewidth}
	\includegraphics[width=.99\linewidth]{supmat_fig3c_parity_string_order_4L_OBC_m0_leg0.pdf}
\end{minipage}
%
\caption{Boundary-parity- and string-order of the QLM Eq.~(1) of the main text at $\mu=0$ for (a) two-leg ladder, $L=200$ (b) three-leg ladder, $L=100$ (c) four-leg ladder, $L=100$ (DMRG data).}
\label{fig:S_stringorder}
\end{figure}
%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{2-leg ladder}

Let us briefly review the results obtained for Eq.~\eqQLM{} of the main text for the two-legs ladder system~(further details of the two-leg ladder model are discussed in Ref.~\cite{Cardarelli2017}). For staggered boundary links, the model becomes symmetric in the mass term $\mu = -\mu$ (note that, as mentioned in the main text, this is not the case for systems with more than two legs). In the ground-state phase diagram three different phases emerge corresponding to the phases obtained in the simplified 1D model of the previous section.

We would like to stress that the 2D-phase diagram conjectured here exhibits certain analogies to the 2-leg ladder case.
The VO and VA phase, proper of the two-leg case and
symmetric under mass sign exchange, generalize in the case of ladders with more
rungs onto the SX/SY and VA/VA' phases.
The graph of the von-Neumann entropy, Fig.~2~d, is reminiscent of the order
parameter graph in the two-leg case (Fig.~2~a of Ref.~\cite{Cardarelli2017}).
However, in the extended case, there appears an emergent deconfined disordered
phase D, which represents one of the main findings of this work and a crucial
element of novelty in comparison with the previous work.

For $\mu=0$ a symmetry protected topological phase~(SPT) emerges. This phase can be well characterized from properties of its entanglement spectrum, as shown in Fig.~\ref{fig:S_entanglement}~(a). The entanglement spectrum $\lambda_i$, which is the ordered sequence of Schmidt eigenvalues obtained for dividing the system into two parts along the central rung of the ladder, exhibits a twofold degeneracy in the SPT phase~\cite{Pollmann2010, Pollmann2012, Pollmann2012A}, as well as vanishing boundary parity and finite string order as shown in Fig.~\ref{fig:S_stringorder}~(a).

\begin{figure}[tb]
\centering
\includegraphics[width=0.7\linewidth]{supmat_fig4_pd_m_Jy_2torus_sv.pdf}
\caption{
Schematic phase diagram of the two-leg cylinder model. Color codes depict the von-Neumann bipartite entanglement entropy $S_{vN}$ of the central rung (infinite DMRG simulation, $\chi=80$). 
\label{fig:S_pd_2torus} }
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Two-leg cylinder}

% xxx correction
For the two-leg cylinder we observe a drastically different ground-state phase diagram, as sketched in Fig.~\ref{fig:S_pd_2torus}. Here, the second order ring-exchange around the cylinder competes with local charge fluctuations in the y-direction and, hence, the mechanism described in the main text to stabilize the distinct phases in the large $|\mu|\gg J_x, J_y$ limit is strongly affected.
In Fig.~\ref{fig:S_pd_2torus} we show numerical results for the phase diagram of the two-leg cylinder as a function of $\mu$ and $J_y$. We only observe two clearly distinct phases. In the $J_y\ll J_x$ and $\mu<0$ regime, we observe a VA phase, which is characterized by an ordering of the link-spins in the y-direction.
The VA phase exhibits a quantum phase transition to a VA' like phase, which is adiabatically connected to the Sy phase. A distinct Sx phase is absent. 





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Three- and four-leg ladder}

In Figs.~\ref{fig:S_entanglement}~(b) and (c) and ~\ref{fig:S_stringorder}~(b) and (c) we present additional data obtained from our DMRG calculations of the three and four-leg ladder systems at $\mu=0$. The exact twofold degeneracy in the entanglement spectrum is lost for more than two legs in the intermediate D phase. However, we observe the emergence of a distinct gap in the entanglement spectrum between a manifold of low lying and higher entanglement states. At the edge the string and parity order exhibit Haldane-like properties in the D phase~(Fig.~\ref{fig:S_stringorder}).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{RK states}

In Fig.~\ref{fig:S_class_plt} we provide further comparisons to the classical RK states, obtained by a simple Metropolis sampling for the classical QLM analogue at infinite temperature, with the DMRG results of the intermediate phase in the QLM. The Metropolis sampling does not mix different symmetry sectors and we make sure that we initialize the algorithm in a state of the gauge vacuum sector compatible with DMRG-simulations. For three- and four-leg ladders and four-leg cylinders the local spin and charge configurations compare very accurately.

\begin{figure}[tb]
\centering
\includegraphics[width=0.99\linewidth]{supmat_fig5_class_flxplt.pdf}
\caption{Comparison of charge and spin configurations obtained from DMRG results for the intermediate phase ($J_x=J_y$, $\mu=0$)  (plots (a),(c), and (e)) to the RK state obtained by an equal amplitude overlap of all connected configurations (plots (b), (d), and (f)) for the four-leg  cylinder (a) and (b), the four-leg ladder (c) and (d), and the three-leg ladder (e) and (f)}
\label{fig:S_class_plt}
\end{figure}


%%
\begin{table}[t]
\vspace{0.5cm}
\centering
\begin{tabular}{ c|c|c } 
  & $J_y^{c,1} \approx $ & $J_y^{c,2} \approx $ \\ 
 \hline
 Simplified 1D model & 0.2 & 1.1 \\ 
 two-leg ladder & 0.5 & 1.3 \\ 
 two-leg cylinder & - & - \\ 
 three-leg ladder & 0.5 & 1.6 \\ 
 four-leg ladder & 0.5 & 1.5 \\ 
 four-leg cylinder & 0.5 & 1.9 \\ 
 \hline
\end{tabular}
\vspace{0.3cm}
\caption{Estimated critical values of the exchange $J_y$ (in units of $J_x=1$) for the phase transition to the intermediate D phase for $\mu=0$.}
\label{tab:S_transitions}
\end{table}
%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Phase diagram}

In Tab.~\ref{tab:S_transitions} we summarize our results on the phase transition points for $\mu=0$ for the different systems analyzed in this work. Increasing the number of legs, the phase transition points behave non-monotonously. However, as a general trend the region of the intermediate phase expands and the transition points seem to approach a ratio $J_y^{c,1} J_y^{c,2} \sim 1$ (in units of $J_x$) as expected for the 2D limit with proper lattice rotation symmetry. 

\begin{figure}[tb]
%
\includegraphics[width=.8\linewidth]{supmat_fig6_energy_4L_PBC_m0_L40_logscale.pdf}
%
\caption{
Energy error $E_{\rm{err}} = E(\chi) - E_0$ as a function of the bond dimension $\chi$, for different values of $J_y$ in the disordered region and close to the phase transitions (four-leg cylinder, $\mu=0$, L=40, DMRG results).
$E_0$ is an estimate of the ground-state energy for a fixed chain length obtained by fitting the data with an exponential curve.
}
\label{fig:energy_vs_chi}
\end{figure}

%
\begin{figure}[tb]
\includegraphics[width=.99\linewidth]{supmat_fig7_gap_legs.pdf}
\caption{Excitation gap $\Delta \epsilon$ (in units of $J_x$) for the (a) 1D toy model, (b) 2-leg (c) 4-leg ladder model within the vacuum gauge-sector at $\mu=0$ for different system sized $L=12$ (crosses), $L=24$ (circle), $L=36$ (plus-symbols). The dotted lines indicate the estimated phase-transition position from Tab.~\ref{tab:S_transitions}.
(d) Excitation gap as a function of the number of legs for $J_y / J_x=1$ (center of the D-phase) and $J_y/J_x=1.8$ (Sy-phase).}
\label{fig:gap_legs}
\end{figure}

The robustness of the disordered phase is confirmed in the limit of large bond-dimension.
In Figure \ref{fig:energy_vs_chi} we show the exponential convergence of the ground-state energy for increasing $\chi$, in the zero mass limit.

This observation can be understood from the calculation of excitation gap $\Delta\epsilon$ as depicted in Fig.~\ref{fig:gap_legs}. Here we show for various geometries and models discussed in the main text the energy gap to the first excited state within the same gauge sector as the ground-state. The data clearly shows that the D-phase exhibits a large excitation gap $\Delta\epsilon \sim 0.4 J_x$. 
Interestingly, this value stays remarkably constant changing the number of legs.
For $J_x/J_y \to 0$ this gap decreases. For the 4-leg ladder system, with partially restored x-y symmetry this is also the case for $J_y/J_x\to 0$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Extended data on the string tension}

In Figs.~\ref{fig:S_pltST_jy0.4}, \ref{fig:S_pltST_jy1.0}, \ref{fig:S_pltST_jy1.8} we finally detail the DMRG results for the calculation of the string-formation and tension between defects as shown in Figs.~(4) and (5) of the main text. 
We depict three examples from the Sy (Fig.~\ref{fig:S_pltST_jy0.4}), D phase (Fig.~\ref{fig:S_pltST_jy1.0}) and Sx (Fig.~\ref{fig:S_pltST_jy1.8}) phases without and with subtraction of the background charge and spin configurations. 

\begin{figure*}[bt]
\centering
\includegraphics[width=0.99\linewidth]{supmat_fig8_pltST_jy0.4.pdf}
\caption{(a) Charge and bond average configurations of two defects with distance $L_D=2$,$4$,$6$,$8$,$12$,$16$ sites. DMRG-data, $L=36$ rungs, $\mu=0.4 J_x$, $J_y / J_x = 0.4$ (b) Same as (a) but after substracting background without charges.}
\label{fig:S_pltST_jy0.4}
\end{figure*}


\begin{figure*}[bt]
\centering
\includegraphics[width=0.99\linewidth]{supmat_fig9_pltST_jy1.0.pdf}
\caption{(a) Charge and bond average configurations of two defects with distance $L_D=2$,$4$,$6$,$8$,$12$,$16$ sites. DMRG-data, $L=36$ rungs, $\mu=0.4 J_x$, $J_y / J_x = 1.0$ (b) Same as (a) but after substracting background without charges.}
\label{fig:S_pltST_jy1.0}
\end{figure*}



\begin{figure*}[bt]
\centering
\includegraphics[width=0.99\linewidth]{supmat_fig10_pltST_jy1.8.pdf}
\caption{(a) Charge and bond average configurations of two defects with distance $L_D=2$,$4$,$6$,$8$,$12$,$16$ sites. DMRG-data, $L=36$ rungs, $\mu=0.4 J_x$, $J_y / J_x = 1.8$ (b) Same as (a) but after substracting background without charges.}
\label{fig:S_pltST_jy1.8}
\end{figure*}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bibliography{references}

\end{document}

